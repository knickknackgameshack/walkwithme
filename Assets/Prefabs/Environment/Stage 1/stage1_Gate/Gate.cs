﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public AudioClip gateClip;
    public AudioSource gateSource;

    public void PlayGate()
    {
        gateSource.clip = gateClip;
        gateSource.Play();
    }
}
