﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portcullis : MonoBehaviour
{
    public AudioClip portcullisClip;
    public AudioSource portcullisSource;

    public void PlayPortcullis()
    {
        portcullisSource.clip = portcullisClip;
        portcullisSource.Play();
    }
}
