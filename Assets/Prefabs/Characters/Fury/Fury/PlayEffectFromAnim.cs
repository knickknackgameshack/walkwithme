﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;
using Character;

public class PlayEffectFromAnim : MonoBehaviour
{
    public EnemyCharacter enemy;
    public GameObject wings;
    public void PlayAttack()
    {
        enemy.activeSource.clip = enemy.attackClip;
        AudioManager.Instance.PlaySFX(enemy.activeSource);
    }

    public void PlayDeath()
    {
        enemy.activeSource.clip = enemy.deathClip;
        AudioManager.Instance.PlaySFX(enemy.activeSource);
        Destroy(wings, 1.0f);
    }
}
