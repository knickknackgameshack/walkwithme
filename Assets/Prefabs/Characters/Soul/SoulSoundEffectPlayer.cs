﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;
using Character;

public class SoulSoundEffectPlayer : MonoBehaviour
{
    public Soul soul;

    public void PlayFootsteps()
    {
        switch(soul.GetMaterial())
        {
            case ("Grass"):
                soul.footstepsSoul.clip = soul.footstepsGrassCobble;
                break;
            case ("Cobble"):
                soul.footstepsSoul.clip = soul.footstepsGrassCobble;
                break;
            case ("Tile"):
                soul.footstepsSoul.clip = soul.footstepsTile;
                break;
            case ("Dirt"):
                soul.footstepsSoul.clip = soul.footstepsDirt;
                break;
            case ("Carpet"):
                soul.footstepsSoul.clip = soul.footstepsCarpet;
                break;
            case ("Stone"):
                soul.footstepsSoul.clip = soul.footstepsGrassCobble;
                break;
        }
        soul.footstepsSoul.Play();
    }
}
