﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverPlay : MonoBehaviour
{
    public Puzzle.LeverSwitch lever;
    private bool firstPlay = true;

    public void PlayLeverAnim()
    {
        if (firstPlay == true)
            firstPlay = false;
        else
        {
            lever.PlayInteractableSFX();
        }
    }
}
