﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle
{
    public class DialSwitch : InteractablePhysicalSwitch
    {
        [SerializeField] public List<bool> states;
        [SerializeField] public int currentIndex;
        public Animator controller;

        public float playTime = 0;
        int[] animeTimes;

        int playamount = 0;
        string animName = "Dial|DialAction";

        private void OnEnable()
        {
            if (states == null)
                states = new List<bool>();
            if (states.Count == 0)
                states.Add(false);
            currentIndex %= states.Count;

            state.State = states[currentIndex];

            controller = this.GetComponent<Animator>();
            controller.StopPlayback();
            controller.speed = states.Count * 3f;
        }

        /// <summary>
        /// Dial switch, has a current index that is incremented when interacted, state is determined by the value of the index according to the states array 
        /// </summary>
        public override void Interact()
        {
            if (playTime > 0) //do not interact while moving
            {
                return;
            }
            controller.Play(animName, 0, 1.0f / states.Count * currentIndex);
            currentIndex = (currentIndex + 1) % states.Count;
            state.State = states[currentIndex];
            
            if (playTime < (4.167f / controller.speed))  playTime += 4.167f / (states.Count) / controller.speed;
            if (playamount < states.Count) playamount ++;
        }

        public void Update()
        {
            if (playTime > 0)
            {
                //if (playTime > (4.167f / controller.speed)) playTime = 4.167f / controller.speed;

                controller.StopPlayback();
                playTime -= Time.deltaTime;
            } 
            else
            {
                controller.StartPlayback();
                playTime = 0;
                
            }
        }

        IEnumerator Spin (int interval)
        {
            controller.Play(animName,0,1.0f/interval);
            yield return new WaitForSeconds(0.1f);
        }
    }
}