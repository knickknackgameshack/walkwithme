﻿namespace Puzzle
{
    public class ActivatableLeverSwitch : LeverSwitch, IActivatableSwitch
    {
        private bool activated;

        public void Activate()
        {
            activated = true;
            //todo show activation
        }

        public override void Interact()
        {
            if (!activated) return;
            base.Interact();
        }

        public void DeActivate()
        {
            activated = false;
            //todo show deactivation
        }
    }
}