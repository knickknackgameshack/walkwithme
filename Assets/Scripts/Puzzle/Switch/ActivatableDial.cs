﻿using System;
using System.Linq;
using OdinSerializer.Utilities;
using UnityEngine;

namespace Puzzle
{
    public class ActivatableDial : DialSwitch, IActivatableSwitch
    {
        [SerializeField] bool activated = false;
        public bool GetActivated => activated;
        [SerializeField] public Material deactivatedMaterial;
        private Material activatedMaterial;
        private Renderer[] renderer;

        private void Awake()
        {
            state.stateChanged += StateChanged;
            renderer = GetComponentsInChildren<Renderer>();
            activatedMaterial = renderer.First().sharedMaterial;
            DeActivate();
        }

        public void Activate()
        {
            stateLight.enabled = true;

            activated = true;
            //todo show activation
            renderer.ForEach(r => r.sharedMaterial = activatedMaterial);
        }

        public override void Interact()
        {
            if (!activated) return;
            base.Interact();
        }

        public void DeActivate()
        {
            stateLight.enabled = false;

            activated = false;
            //todo show deactivation
            renderer.ForEach(r => r.sharedMaterial = deactivatedMaterial);
        }
    }
}