﻿using Puzzle;

namespace DefaultNamespace
{
    public class ActivatableButtonSwitch : ButtonSwitch, IActivatableSwitch
    {
        private bool activated;

        public void Activate()
        {
            activated = true;
        }

        public override void Interact()
        {
            if (!activated) return;
            base.Interact();
        }

        public void DeActivate()
        {
            activated = false;
        }
    }
}