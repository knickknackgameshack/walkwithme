﻿using System.Collections;
using UnityEngine;

namespace Puzzle
{
    public class ButtonSwitch : InteractablePhysicalSwitch
    {
        private bool interacted;
        public float ActiveTime;

        public override void Interact()
        {
            if (!interacted)
            {
                interacted = true;
                StartCoroutine(TimeToInteract(ActiveTime));
                state.State = true;
            }
        }

        private IEnumerator TimeToInteract(float time)
        {
            yield return new WaitForSeconds(time);
            state.State = false;
            interacted = false;
        }
    }
}