﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle
{
    public class LeverSwitch : InteractablePhysicalSwitch
    {

        private Queue<IEnumerator> coroutineQueue = new Queue<IEnumerator>();
        private Animator animController;
        private bool animating;

        /// <summary>
        /// Simple lever switch, just changes the state to !state
        /// </summary>
        public override void Interact()
        {
            if (animating) return;
            //state.State = !state.State;
            if (!state.State) coroutineQueue.Enqueue(LeverActivate());
            else coroutineQueue.Enqueue(LeverDeactivate());
        }

        public void Awake()
        {
            animController = this.transform.GetComponentInChildren<Animator>();
            StartCoroutine(CoroutineCoordinator());
        }

        IEnumerator CoroutineCoordinator()
        {
            while (true)
            {
                while (coroutineQueue.Count > 0)
                    yield return StartCoroutine(coroutineQueue.Dequeue());
                yield return null;
            }
        }

        IEnumerator LeverActivate()
        {
            animating = true;
            animController.SetBool("activate", true);
            yield return new WaitForSeconds(0.46f);
            state.State = !state.State;
            animating = false;
        }

        IEnumerator LeverDeactivate()
        {
            animating = true;
            animController.SetBool("activate", false);
            yield return new WaitForSeconds(0.46f);
            state.State = !state.State;
            animating = false;
        }
    }
}