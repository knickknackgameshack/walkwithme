﻿using System;
using UnityEngine;

namespace Puzzle
{
    public abstract class Switch : MonoBehaviour
    {
        [SerializeField] public SwitchState state;

        // inverts the output of the state if true


        void Start()
        {
            state.State = state.State;
        }
    }

    [Serializable]
    public struct SwitchState
    {
        [SerializeField] private bool state;
        [SerializeField] public bool not;

        public bool State
        {
            get { return (state != not); }
            set
            {
                state = (value != not);
                stateChanged?.Invoke(state);
            }
        }

        public bool ProperState => (state == !not);

        public Action<bool> stateChanged;
    }
}