﻿using Puzzle.Editor;
using UnityEditor;
using UnityEngine;

namespace Puzzle
{
    [CustomEditor(typeof(ActivatableDial))]
    public class ActivatableDialEditor : DialSwitchEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var activatable = target as ActivatableDial;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Activatable Dial Material");
            var tempMaterial =
                (Material) EditorGUILayout.ObjectField(activatable.deactivatedMaterial, typeof(Material), false);
            if (activatable.deactivatedMaterial != tempMaterial)
            {
                activatable.deactivatedMaterial = tempMaterial;
            }
        }
    }
}