﻿using UnityEditor;
using UnityEngine;

namespace Puzzle.Editor
{
    [CustomEditor(typeof(Switch), true)]
    public class SwitchEditor : UnityEditor.Editor
    {
        protected GUIStyle redFont => new GUIStyle(GUI.skin.label)
            {normal = {textColor = Color.red * 0.6f}, fontStyle = FontStyle.Bold};


        protected GUIStyle bold => new GUIStyle(GUI.skin.label)
            { };

        protected GUIStyle normal => new GUIStyle(GUI.skin.label)
            {fontStyle = FontStyle.BoldAndItalic, contentOffset = new Vector2(10, 0)};

        protected GUIStyle greenFont => new GUIStyle(GUI.skin.label)
            {normal = {textColor = Color.green * 0.6f}, fontStyle = FontStyle.Bold};

        public override void OnInspectorGUI()
        {
            var swtch = target as Switch;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("State");
            if (swtch.state.State) EditorGUILayout.LabelField("True", greenFont);
            else EditorGUILayout.LabelField("False", redFont);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button(swtch.state.not ? "Inverted" : "Normal"))
            {
                swtch.state.not = !swtch.state.not;
                EditorUtility.SetDirty(swtch);
            }

            if (swtch is InteractablePhysicalSwitch intSwitch && GUILayout.Button("Interact"))
            {
                intSwitch.Interact();
                EditorUtility.SetDirty(intSwitch);
            }
        }
    }
}