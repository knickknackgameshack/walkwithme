﻿using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

namespace Puzzle.Editor
{
    [CustomEditor(typeof(PressurePlateSwitch), true)]
    public class PressurePlateSwitchEditor : InteractableSwitchEditor
    {
        private PressurePlateSwitch.SensorType sensorType;
        private bool requireSpecific;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            var pressurePlate = target as PressurePlateSwitch;
            sensorType = pressurePlate.sensorType;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Activates On:");
            sensorType = (PressurePlateSwitch.SensorType) EditorGUILayout.EnumPopup(sensorType);
            EditorGUILayout.EndHorizontal();

            if (sensorType == PressurePlateSwitch.SensorType.PuzzleObject)
            {
                requireSpecific = EditorGUILayout.ToggleLeft("Require Specific Puzzle Object", requireSpecific);

                if (requireSpecific)
                {
                    var specificObject =
                        (PuzzleObject) EditorGUILayout.ObjectField(pressurePlate.puzzleObject, typeof(PuzzleObject),
                            true);
                    if (specificObject != pressurePlate.puzzleObject)
                    {
                        pressurePlate.puzzleObject = specificObject;
                        EditorUtility.SetDirty(target);
                    }
                }
                else if (pressurePlate.puzzleObject != null)
                {
                    pressurePlate.puzzleObject = null;
                    EditorUtility.SetDirty(target);
                }
            }

            if (sensorType != pressurePlate.sensorType)
            {
                pressurePlate.sensorType = sensorType;
                EditorUtility.SetDirty(pressurePlate);
            }
        }
    }
}