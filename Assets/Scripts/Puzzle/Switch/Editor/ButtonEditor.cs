﻿using UnityEditor;

namespace Puzzle.Editor
{
    [CustomEditor(typeof(ButtonSwitch))]
    public class ButtonEditor : InteractableSwitchEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var button = target as ButtonSwitch;

            var activeTime = button.ActiveTime;
            activeTime = EditorGUILayout.Slider("Active Time", activeTime, 30, 0.5f);
            if (activeTime != button.ActiveTime)
            {
                button.ActiveTime = activeTime;
                EditorUtility.SetDirty(button);
            }
        }
    }
}