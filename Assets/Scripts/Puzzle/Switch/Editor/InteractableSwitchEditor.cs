﻿using Puzzle.Editor;
using UnityEditor;
using UnityEngine;

namespace Puzzle.Editor
{
    [CustomEditor(typeof(PhysicalSwitch), true)]
    public class InteractableSwitchEditor : SwitchEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var intSwitch = target as PhysicalSwitch;

            var light = (Light) EditorGUILayout.ObjectField("Status Light", intSwitch.stateLight, typeof(Light), true);

            if (light != intSwitch.stateLight)
            {
                intSwitch.stateLight = light;
                EditorUtility.SetDirty(target);
            }
            EditorGUILayout.BeginHorizontal();

            var interactClip = (AudioClip)EditorGUILayout.ObjectField("Interact Clip:", intSwitch.interactClip,
                typeof(AudioClip), true);
            if (interactClip != intSwitch.interactClip)
            {
                intSwitch.interactClip = (AudioClip)interactClip;
                EditorUtility.SetDirty(intSwitch);
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            var interactSource = (AudioSource)EditorGUILayout.ObjectField("Interact Source:", intSwitch.interactSource,
                typeof(AudioSource), true);
            if (interactSource != intSwitch.interactSource)
            {
                intSwitch.interactSource = (AudioSource)interactSource;
                EditorUtility.SetDirty(intSwitch);
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            var enumType = (PhysicalSwitch.SwitchType)EditorGUILayout.EnumPopup("Switch Type: ", intSwitch.switchType);
            if (enumType != intSwitch.switchType)
            {
                intSwitch.switchType = (PhysicalSwitch.SwitchType)enumType;
                EditorUtility.SetDirty(intSwitch);
            }

            EditorGUILayout.EndHorizontal();
        }
    }
}