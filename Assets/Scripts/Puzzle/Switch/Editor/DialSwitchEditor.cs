﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

namespace Puzzle.Editor
{
    [CustomEditor(typeof(DialSwitch), true)]
    public class DialSwitchEditor : InteractableSwitchEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var dialSwitch = target as DialSwitch;
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Add State"))
            {
                dialSwitch.states.Add(false);
                EditorUtility.SetDirty(dialSwitch);
            }

            if (GUILayout.Button("<", GUILayout.Width(30)))
            {
                dialSwitch.currentIndex =
                    ((dialSwitch.currentIndex - 1) + dialSwitch.states.Count) % dialSwitch.states.Count;
                EditorUtility.SetDirty(dialSwitch);
            }

            if (GUILayout.Button(">", GUILayout.Width(30)))
            {
                dialSwitch.currentIndex = (dialSwitch.currentIndex + 1) % dialSwitch.states.Count;
                EditorUtility.SetDirty(dialSwitch);
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();


            for (int i = 0; i < dialSwitch.states.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();


                EditorGUILayout.LabelField((i + 1).ToString(),
                    dialSwitch.currentIndex == i ? normal : bold, GUILayout.Width(40));

                if (GUILayout.Button(dialSwitch.states[i] ? "True" : "False"))
                {
                    dialSwitch.states[i] = !dialSwitch.states[i];
                    EditorUtility.SetDirty(dialSwitch);
                }

                if (GUILayout.Button("-", GUILayout.Width(30)))
                {
                    dialSwitch.states.RemoveAt(i);
                    if (dialSwitch.states.Count == 0) dialSwitch.currentIndex = 0;
                    else dialSwitch.currentIndex %= dialSwitch.states.Count;
                    EditorUtility.SetDirty(dialSwitch);
                    break;
                }

                EditorGUILayout.EndHorizontal();
            }


            var controller = (Animator)EditorGUILayout.ObjectField("Animator:", dialSwitch.controller,
                typeof(Animator), true);
            if (controller != dialSwitch.controller)
            {
                dialSwitch.controller = (Animator)controller;
                EditorUtility.SetDirty(dialSwitch);
            }

        }
    }
}