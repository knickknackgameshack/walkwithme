﻿using System;
using Character;
using UnityEngine;

namespace Puzzle
{
    [RequireComponent(typeof(BoxCollider))]
    public class PressurePlateSwitch : PhysicalSwitch
    {
        public enum SensorType
        {
            None,
            Character,
            Soul,
            SoulOrPuzzleObject,
            PuzzleObject
        }

        [SerializeField, Tooltip("The things which can activate this pressure plate")]
        public SensorType sensorType;

        [SerializeField] private BoxCollider collider;

        public PuzzleObject puzzleObject;

        private void Reset()
        {
            collider = !GetComponent<BoxCollider>()
                ? gameObject.AddComponent<BoxCollider>()
                : GetComponent<BoxCollider>();
        }


        private void OnTriggerEnter(Collider other)
        {
            if (state.State != state.not) return;
            //check if the collider is an applicable object
            switch (sensorType)
            {
                case SensorType.None: //nothing will activate it, always outputs the same value
                    break;
                case SensorType.Soul: //only the soul, or a clone of the soul will activate it
                    if (other.GetComponent<Soul>())
                        state.State = !state.not;
                    break;
                case SensorType.Character: //any character will activate it including reaper and enemies
                    if (other.GetComponent<Character.Character>())
                        state.State = !state.not;
                    break;
                case SensorType.PuzzleObject: //only puzzle objects will activate it
                    if (other.GetComponent<PuzzleObject>() is PuzzleObject puzzle)
                    {
                        if (puzzleObject != null && puzzle != puzzleObject)
                        {
                            break;
                        }

                        state.State = !state.not;
                    }

                    break;
                case SensorType.SoulOrPuzzleObject:
                    if (other.GetComponent<Character.Character>() || other.GetComponent<PuzzleObject>())
                        state.State = !state.not;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            //if there is a collider that still meets our criteria in the trigger, then we can ignore the one leaving
            var colliders = Physics.OverlapBox(transform.TransformPoint(collider.center), collider.size / 2,
                transform.rotation);

            foreach (var coll in colliders)
            {
                switch (sensorType)
                {
                    case SensorType.None: //nothing will activate it, always outputs the same value
                        break;
                    case SensorType.Soul: //only the soul, or a clone of the soul will activate it
                        if (coll.GetComponent<Soul>())
                            return;
                        break;
                    case SensorType.Character: //any character will activate it including reaper and enemies
                        if (coll.GetComponent<Character.Character>())
                            return;
                        break;
                    case SensorType.PuzzleObject: //only puzzle objects will activate it
                        if (coll.GetComponent<PuzzleObject>() is PuzzleObject puzzle)
                        {
                            if (puzzleObject != null && puzzle != puzzleObject)
                            {
                                break;
                            }

                            return;
                        }

                        break;
                    case SensorType.SoulOrPuzzleObject:
                        if (other.GetComponent<Character.Character>() || other.GetComponent<PuzzleObject>())
                            return;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            //if we get here that means that the one leaving was the only one activating it, therefore, we can safely set to the default value
            state.State = state.not;
        }

        private void OnEnable()
        {
            var coll = GetComponent<Collider>();
            if (!coll.isTrigger)
            {
                coll.isTrigger = true;
            }
        }
    }
}