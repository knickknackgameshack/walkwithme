﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle
{
    public abstract class PhysicalSwitch : Switch
    {
        public Light stateLight;

        public AudioClip interactClip;
        public AudioSource interactSource;

        public enum SwitchType { Dial, Lever, Pressure, Altar }
        [SerializeField] public SwitchType switchType;

        private void Awake()
        {
            state.stateChanged += StateChanged;
        }

        protected void StateChanged(bool newState)
        {
            if (stateLight != null)
            {
                stateLight.color = state.State ? Color.green : Color.red;
            }
        }
    }
}