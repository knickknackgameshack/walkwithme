﻿using System.Collections.Generic;
using Character;
using UnityEngine;

namespace Puzzle
{
    public abstract class InteractablePhysicalSwitch : PhysicalSwitch
    {
        private List<PlayerCharacter> players = new List<PlayerCharacter>();


        //add player to list of tracked players and subscribe to interaction
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && other.GetComponent<PlayerCharacter>() is Soul soul)
            {
                players.Add(soul);
                soul.Interacted += Interact;
            }
            if (other.CompareTag("Player") && other.GetComponent<PlayerCharacter>() is Reaper reaper)
            {
                reaper.EnableSoulLegoHead();
            }
        }

        public void PlayInteractableSFX()
        {
            interactSource.clip = interactClip;
            interactSource.Play();
        }

        //remove player from list of tracked players and unsubscribe from interaction
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player") && other.GetComponent<PlayerCharacter>() is Soul soul)
            {
                if (players.Contains(soul))
                {
                    soul.Interacted -= Interact;
                    players.Remove(soul);
                }
            }
            if (other.CompareTag("Player") && other.GetComponent<PlayerCharacter>() is Reaper reaper)
            {
                reaper.DisableSoulLegoHead();
            }
        }

        public abstract void Interact();

        private void Interact(PlayerCharacter playerCharacter) => Interact();
    }
}