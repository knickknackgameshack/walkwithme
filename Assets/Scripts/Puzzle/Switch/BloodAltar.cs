﻿using System;
using System.Collections.Generic;
using Character;
using UnityEngine;

namespace Puzzle
{
    public class BloodAltar : PhysicalSwitch
    {
        private List<EnemyCharacter> enemies = new List<EnemyCharacter>();
        private bool activated;

        private void OnTriggerEnter(Collider other)
        {
            if (!activated && other.CompareTag("Enemy"))
            {
                var enemy = other.GetComponent<EnemyCharacter>();
                if (!enemies.Contains(enemy))
                {
                    enemies.Add(enemy);
                    enemy.Died += Died;
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!activated && other.CompareTag("Enemy"))
            {
                var enemy = other.GetComponent<EnemyCharacter>();
                if (enemies.Contains(enemy))
                {
                    enemies.Remove(enemy);
                    enemy.Died -= Died;
                }
            }
        }

        private void Died(Character.Character character)
        {
            activated = true;
            foreach (var enemy in enemies)
            {
                enemy.Died -= Died;
            }

            enemies = new List<EnemyCharacter>();

            state.State = true;
        }
    }
}