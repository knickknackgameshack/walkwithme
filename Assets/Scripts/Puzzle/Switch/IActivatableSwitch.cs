﻿using UnityEngine;

namespace Puzzle
{
    public interface IActivatableSwitch
    {
        new void Activate();
        new void DeActivate();
    }
}