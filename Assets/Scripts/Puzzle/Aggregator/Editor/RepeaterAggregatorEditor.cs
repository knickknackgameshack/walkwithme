﻿using UnityEditor;

namespace Puzzle.Editor
{
    [CustomEditor(typeof(RepeaterAggregator))]
    public class RepeaterAggregatorEditor : AggregatorEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var repeater = target as RepeaterAggregator;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Lock Switch");
            var lockSwitch = repeater.LockState;
            lockSwitch = (Switch) EditorGUILayout.ObjectField(@lockSwitch, typeof(Switch), true);

            if (lockSwitch != repeater.LockState)
            {
                repeater.LockState = lockSwitch;
                EditorUtility.SetDirty(repeater);
            }
        }
    }
}