﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Puzzle.Editor
{
    [CustomEditor(typeof(Aggregator), true)]
    public class AggregatorEditor : SwitchEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var aggregator = target as Aggregator;
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Switch"))
            {
                aggregator.switches.Add(null);
                EditorUtility.SetDirty(aggregator);
            }

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();


            if (aggregator.switches == null)
            {
                aggregator.switches = new List<Switch>();
                EditorUtility.SetDirty(aggregator);
            }

            for (int i = 0; i < aggregator.switches.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();


                EditorGUILayout.LabelField((i + 1).ToString(), GUILayout.Width(30));

                var @switch = aggregator.switches[i];

                @switch = (Switch) EditorGUILayout.ObjectField(@switch, typeof(Switch), true);

                if (@switch != aggregator.switches[i])
                {
                    aggregator.switches[i] = @switch;
                    EditorUtility.SetDirty(aggregator);
                }

                if (GUILayout.Button("-", GUILayout.Width(30)))
                {
                    aggregator.switches.RemoveAt(i);
                    EditorUtility.SetDirty(aggregator);
                    break;
                }

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}