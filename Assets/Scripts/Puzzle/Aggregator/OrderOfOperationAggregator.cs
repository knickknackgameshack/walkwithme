﻿using System.Linq;

namespace Puzzle
{
    public class OrderOfOperationAggregator : Aggregator
    {
        protected override void StateChanged(bool state)
        {
            for (var i = 0; i < switches.Count - 1; i++)
            {
                var curSwitch = switches[i];
                var nextSwitch = switches[i + 1];

                if (!(nextSwitch is IActivatableSwitch activatableSwitch)) continue;
                if (curSwitch.state.State)
                {
                    activatableSwitch.Activate();
                }
                else
                {
                    activatableSwitch.DeActivate();
                }
            }

            this.state.State = switches.TrueForAll(s => s.state.State);
        }
    }
}