﻿using Controller;
using TMPro;
using UnityEngine;

namespace Puzzle
{
    public class StateDisplay : Aggregator
    {
        [SerializeField] private TextMeshPro text;

        private void Start()
        {
            text.transform.rotation = Quaternion.LookRotation(CameraController.Forward);
        }

        protected override void StateChanged(bool state)
        {
            text.text = state.ToString();
        }
    }
}