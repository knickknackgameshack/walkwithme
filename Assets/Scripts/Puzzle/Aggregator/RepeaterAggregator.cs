﻿using System;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Puzzle
{
    public class RepeaterAggregator : Aggregator
    {
        [SerializeField] public Switch LockState;

        private void Awake()
        {
            if (switches.Count > 1)
            {
                Debug.LogWarning("Unintentional consequences my come from using more than one input on a repeater",
                    this);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            LockState.state.stateChanged += LockStateChanged;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            LockState.state.stateChanged -= LockStateChanged;
        }

        private void LockStateChanged(bool state)
        {
            if (state == false)
            {
                //the state changed from true to false, change this state to match the other states
                if (switches.TrueForAll(s => s.state.State))
                {
                    this.state.State = true;
                }
                else
                {
                    this.state.State = false;
                }
            }
        }

        protected override void StateChanged(bool state)
        {
            if (LockState == null || !LockState.state.State)
            {
                this.state.State = state;
            }
        }
    }
}