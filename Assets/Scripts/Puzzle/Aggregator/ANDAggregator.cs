﻿namespace Puzzle
{
    public class ANDAggregator : Aggregator
    {
        protected override void StateChanged(bool state)
        {
            if (switches.TrueForAll(s => s.state.State))
            {
                this.state.State = !this.state.not;
            }
            else
            {
                this.state.State = this.state.not;
            }
        }
    }
}