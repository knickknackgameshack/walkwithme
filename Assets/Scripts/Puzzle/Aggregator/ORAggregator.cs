﻿namespace Puzzle
{
    public class ORAggregator : Aggregator
    {
        protected override void StateChanged(bool state)
        {
            if (switches.Exists(s => s.state.State == true))
            {
                this.state.State = !this.state.not;
            }
            else
            {
                this.state.State = this.state.not;
            }
        }
    }
}