﻿using System.Linq;

namespace Puzzle
{
    public class XORAggregator : Aggregator
    {
        protected override void StateChanged(bool state)
        {
            if (switches.Count(s => s.state.State) % 2 == 1)
            {
                this.state.State = !this.state.not;
            }
            else
            {
                this.state.State = this.state.not;
            }
        }
    }
}