﻿using System.Collections.Generic;
using UnityEngine;

namespace Puzzle
{
    public abstract class Aggregator : Switch
    {
        [SerializeField] public List<Switch> switches;


        protected virtual void OnEnable()
        {
            foreach (var @switch in switches)
            {
                @switch.state.stateChanged += StateChanged;
            }
        }

        protected virtual void OnDisable()
        {
            foreach (var @switch in switches)
            {
                @switch.state.stateChanged -= StateChanged;
            }
        }

        /// <summary>
        /// Determines the state of this aggregator, and changes the state to match
        /// </summary>
        /// <param name="state"></param>
        protected abstract void StateChanged(bool state);
    }
}