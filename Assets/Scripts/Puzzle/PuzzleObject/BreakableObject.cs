﻿using System.Collections.Generic;
using Character;
using UnityEngine;

namespace Puzzle
{
    public class BreakableObject : MonoBehaviour
    {
        [SerializeField] private AudioSource sound;

        [SerializeField] private Collider collider;
        [SerializeField] private Collider trigger;

        private List<PlayerCharacter> players;

        void Awake()
        {
            players = new List<PlayerCharacter>();
            gameObject.layer = (int) Game.Layer.Breakable;
        }

        public void Break()
        {
            int num = gameObject.transform.childCount;

            for (var index = 0; index < num; index++)
            {
                var childRD = gameObject.transform.GetChild(index).GetComponent<Rigidbody>();
                var childColl = childRD.gameObject.GetComponent<Collider>();
                childColl.enabled = true;
                childRD.isKinematic = false;

                Destroy(childRD.gameObject, 10);
            }

            collider.enabled = false;
            trigger.enabled = false;
            RemoveHeads();
        }

        private void RemoveHeads()
        {
            players.ForEach(playerCharacter =>
            {
                if (playerCharacter is Reaper reaper)
                {
                    //todo hide attack button
                }
                else if (playerCharacter is Soul soul)
                {
                    soul.DisableReaperLegoHead();
                }
            });
            players = new List<PlayerCharacter>();
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<PlayerCharacter>() is PlayerCharacter player)
            {
                players.Add(player);
                if (player is Reaper reaper)
                {
                    //todo show attack button
                }
                else if (player is Soul soul)
                {
                    soul.EnableReaperLegoHead();
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<PlayerCharacter>() is PlayerCharacter player && players.Contains(player))
            {
                players.Remove(player);
                if (player is Reaper reaper)
                {
                    //todo hide attack button
                }
                else if (player is Soul soul)
                {
                    soul.DisableReaperLegoHead();
                }
            }
        }
    }
}