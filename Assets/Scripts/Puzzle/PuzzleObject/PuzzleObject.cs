﻿using System;
using System.Collections.Generic;
using Character;
using UnityEngine;

namespace Puzzle
{
    [RequireComponent(typeof(Rigidbody))]
    public class PuzzleObject : MonoBehaviour
    {
        private List<PlayerCharacter> players = new List<PlayerCharacter>();

        private bool carried;

        private Rigidbody rgd;
        private FixedJoint fixedJoint;

        private void Awake()
        {
            rgd = GetComponent<Rigidbody>();
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && other.GetComponent<PlayerCharacter>() is Soul soul)
            {
                players.Add(soul);
                soul.Interacted += Interact;
            }
        }

        private void Interact(PlayerCharacter obj)
        {
            if (carried && fixedJoint != null)
            {
                Drop();
                carried = false;
            }
            else if (carried == false)
            {
                Pickup(obj);
                carried = true;
            }
        }

        private void Pickup(PlayerCharacter player)
        {
            rgd.detectCollisions = false;

            transform.position =
                Vector3.up * 0.15f +
                player.transform.forward / 2 +
                player.transform.position +
                player.transform.up * 0.05f;
            transform.rotation = player.transform.rotation;

            rgd.velocity = Vector3.zero;
            rgd.angularVelocity = Vector3.zero;


            fixedJoint = gameObject.AddComponent<FixedJoint>();
            fixedJoint.enablePreprocessing = true;
            fixedJoint.connectedBody = player.Rigidbody;
            //fixedJoint.autoConfigureConnectedAnchor = false;
            //fixedJoint.connectedAnchor = Vector3.up * 0.25f + player.transform.forward / 1.5f;
            fixedJoint.connectedMassScale = 0;
            fixedJoint.enableCollision = false;

            rgd.detectCollisions = true;
        }

        private void Drop()
        {
            if (carried && fixedJoint != null)
            {
                //drop
                Destroy(fixedJoint);
                carried = false;
                rgd.WakeUp();
                //rgd.isKinematic = false;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player") && other.GetComponent<PlayerCharacter>() is Soul soul)
            {
                if (players.Contains(soul))
                {
                    soul.Interacted -= Interact;
                    players.Remove(soul);
                }

                if (players.Count == 0)
                {
                    Drop();
                }
            }
        }
    }
}