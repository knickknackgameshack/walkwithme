﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Puzzle.Trigger.Editor
{
    [CustomEditor(typeof(RNGTrigger))]
    public class RNGTriggerEditor : TriggerEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var rng = target as RNGTrigger;

            EditorGUILayout.Separator();

            var dial = rng.DialSwitch;
            dial = (ActivatableDial) EditorGUILayout.ObjectField("Dial Switch", dial, typeof(ActivatableDial), true);
            if (dial != rng.DialSwitch)
            {
                rng.DialSwitch = dial;
                EditorUtility.SetDirty(rng);
            }


            EditorGUILayout.Separator();


            if (rng.ProxySwitches == null)
            {
                rng.ProxySwitches = new List<ProxySwitch>();
                EditorUtility.SetDirty(rng);
            }

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Proxy Switch"))
            {
                var proxySwitch = new GameObject {name = "Proxy Switch"};
                var proxy = proxySwitch.AddComponent<ProxySwitch>();
                proxySwitch.transform.parent = rng.transform;
                rng.ProxySwitches.Add(proxy);
                EditorUtility.SetDirty(rng);
            }

            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < rng.ProxySwitches.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();


                EditorGUILayout.LabelField((i + 1).ToString(), GUILayout.Width(40));

                //todo object field
                var on = rng.ProxySwitches[i].state.ProperState;
                EditorGUILayout.LabelField(on ? "True" : "False", on ? greenFont : redFont);
                if (GUILayout.Button("-", GUILayout.Width(30)))
                {
                    var proxyS = rng.ProxySwitches[i];
                    DestroyImmediate(proxyS.gameObject);
                    rng.ProxySwitches.RemoveAt(i);
                    EditorUtility.SetDirty(rng);
                    break;
                }

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}