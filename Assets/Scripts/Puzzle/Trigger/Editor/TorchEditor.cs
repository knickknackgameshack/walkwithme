﻿using UnityEditor;
using UnityEngine;
using UnityEngine.VFX;

namespace Puzzle.Trigger.Editor
{
    [CustomEditor(typeof(Torch))]
    public class TorchEditor : TriggerEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var torch = target as Torch;


            EditorGUILayout.Separator();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical();
            //Light
            EditorGUILayout.LabelField("Light", bold);

            var light = torch.gameObject.GetComponentInChildren<Light>();
            if (torch.light != light)
            {
                torch.light = light;
                EditorUtility.SetDirty(torch);
            }

            EditorGUILayout.LabelField(torch.light != null ? "Found" : "Not Found",
                torch.light != null ? greenFont : redFont);


            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            //Particles
            EditorGUILayout.LabelField("Particle System", bold);

            var vfx = torch.gameObject.GetComponentInChildren<VisualEffect>();
            if (torch.vfx != vfx)
            {
                torch.vfx = vfx;
                EditorUtility.SetDirty(torch);
            }

            EditorGUILayout.LabelField(torch.vfx != null ? "Found" : "Not Found",
                torch.vfx != null ? greenFont : redFont);
            //
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();
        }
    }
}