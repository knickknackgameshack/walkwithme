﻿using UnityEditor;
using UnityEngine.Playables;

namespace Puzzle.Trigger.Editor
{
    [CustomEditor(typeof(TimelineActivator))]
    public class TimelineActivatorEditor : TriggerEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var timelineActivator = target as TimelineActivator;

            EditorGUILayout.LabelField("Playable Director");
            var playableDirector = timelineActivator.playable;
            playableDirector =
                (PlayableDirector) EditorGUILayout.ObjectField(playableDirector, typeof(PlayableDirector), true);
            if (playableDirector != timelineActivator.playable)
            {
                timelineActivator.playable = playableDirector;
                EditorUtility.SetDirty(target);
            }
        }
    }
}