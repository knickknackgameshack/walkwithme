﻿using UnityEditor;
using UnityEngine;

namespace Puzzle.Trigger.Editor
{
    [CustomEditor(typeof(Trigger))]
    public class TriggerEditor : UnityEditor.Editor
    {
        protected GUIStyle redFont => new GUIStyle(GUI.skin.label)
            {normal = {textColor = Color.red * 0.6f}, fontStyle = FontStyle.Bold};


        protected GUIStyle bold => new GUIStyle(GUI.skin.label)
            {fontStyle = FontStyle.Bold};

        protected GUIStyle greenFont => new GUIStyle(GUI.skin.label)
            {normal = {textColor = Color.green * 0.6f}, fontStyle = FontStyle.Bold};

        public override void OnInspectorGUI()
        {
            var trigger = target as Trigger;

            var @switch = trigger.switchObject;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("State");
            if (@switch != null)
            {
                EditorGUILayout.LabelField(@switch.state.ProperState ? "Activated" : "Deactivated",
                    @switch.state.ProperState ? greenFont : redFont);
            }
            else
            {
                EditorGUILayout.LabelField("Switch Not Found", redFont);
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Switch Object");
            @switch = (Switch) EditorGUILayout.ObjectField(@switch, typeof(Switch), true);
            EditorGUILayout.EndHorizontal();

            if (@switch != trigger.switchObject)
            {
                trigger.switchObject = @switch;
                EditorUtility.SetDirty(target);
            }
        }
    }
}