﻿using System;
using System.Collections.Generic;
using System.Linq;
using Character;
using Game;
using UnityEditor;
using UnityEngine;

namespace Puzzle.Trigger.Editor
{
    [CustomEditor(typeof(EnemySpawner))]
    public class EnemySpawnerEditor : TriggerEditor
    {
        private bool spwnPoints;

        private bool spawnDelay;
        private bool enemyDisplay;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var enemySpawner = target as EnemySpawner;

            EditorGUILayout.Separator();


            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Number of Enemies");
            var numEnemies = EditorGUILayout.IntField(enemySpawner.NumberOfEnemies);
            if (numEnemies != enemySpawner.NumberOfEnemies)
            {
                enemySpawner.NumberOfEnemies = numEnemies;
                EditorUtility.SetDirty(target);
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Enemy");
            var enemy = (EnemyCharacter) EditorGUILayout.ObjectField(enemySpawner.EnemyPrefab, typeof(EnemyCharacter),
                false);
            if (enemy != enemySpawner.EnemyPrefab)
            {
                enemySpawner.EnemyPrefab = enemy;
                EditorUtility.SetDirty(target);
            }

            EditorGUILayout.EndHorizontal();

            enemyDisplay = EditorGUILayout.Foldout(enemyDisplay, "Enemy Spawn Display");
            if (enemyDisplay)
            {
                var tempEnemyDisplayDelay = enemySpawner.AmountToShowEnemySpawn;

                EditorGUILayout.BeginHorizontal();
                tempEnemyDisplayDelay.x = EditorGUILayout.FloatField("Min: ", tempEnemyDisplayDelay.x);
                tempEnemyDisplayDelay.y = EditorGUILayout.FloatField("Max: ", tempEnemyDisplayDelay.y);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.MinMaxSlider(ref tempEnemyDisplayDelay.x, ref tempEnemyDisplayDelay.y, 0, 10);


                if (tempEnemyDisplayDelay.x > tempEnemyDisplayDelay.y)
                    tempEnemyDisplayDelay.y = tempEnemyDisplayDelay.x;

                if (tempEnemyDisplayDelay != enemySpawner.AmountToShowEnemySpawn)
                {
                    enemySpawner.AmountToShowEnemySpawn = tempEnemyDisplayDelay;
                    EditorUtility.SetDirty(target);
                }

                EditorGUILayout.Space();

                var displayEnemyObject = enemySpawner.ShowEnemySpawnObject;

                displayEnemyObject =
                    (GameObject) EditorGUILayout.ObjectField(displayEnemyObject, typeof(GameObject), false);

                if (displayEnemyObject != enemySpawner.ShowEnemySpawnObject)
                {
                    enemySpawner.ShowEnemySpawnObject = displayEnemyObject;
                    EditorUtility.SetDirty(target);
                }
            }


            spawnDelay = EditorGUILayout.Foldout(spawnDelay, "Spawn Delay");
            if (spawnDelay)
            {
                var tempDelay = enemySpawner.SpawnDelay;

                EditorGUILayout.BeginHorizontal();
                tempDelay.x = EditorGUILayout.FloatField("Min: ", tempDelay.x);
                tempDelay.y = EditorGUILayout.FloatField("Max: ", tempDelay.y);
                EditorGUILayout.EndHorizontal();

                if (tempDelay.x > tempDelay.y) tempDelay.y = tempDelay.x;

                EditorGUILayout.MinMaxSlider(ref tempDelay.x, ref tempDelay.y, 0, 60);

                if (tempDelay != enemySpawner.SpawnDelay)
                {
                    enemySpawner.SpawnDelay = tempDelay;
                    EditorUtility.SetDirty(target);
                }
            }

            EditorGUILayout.Separator();

            spwnPoints = EditorGUILayout.Foldout(spwnPoints, "Spawn Points");
            if (spwnPoints)
            {
                if (GUILayout.Button("Add Point"))
                {
                    var patrolPoint = new Vector3();

                    enemySpawner.spawnPoints.Add(patrolPoint);
                    EditorUtility.SetDirty(target);
                }

                for (int i = 0; i < enemySpawner.spawnPoints.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();

                    Vector3 point = enemySpawner.spawnPoints[i];

                    point.x = EditorGUILayout.FloatField(point.x);
                    point.y = EditorGUILayout.FloatField(point.y);
                    point.z = EditorGUILayout.FloatField(point.z);

                    enemySpawner.spawnPoints[i] = point;

                    if (GUILayout.Button("-"))
                    {
                        enemySpawner.spawnPoints.RemoveAt(i);
                        EditorUtility.SetDirty(target);
                    }

                    EditorGUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.Separator();


            var levelManager = FindObjectsOfType<LevelManager>()
                .FirstOrDefault(l => l.LevelName == enemySpawner.gameObject.scene.name);
            var levelManagerIndex = enemySpawner.patrolPathIndex < -1 ? -1 : enemySpawner.patrolPathIndex + 1;

            var tempNames = levelManager.patrolPoints.Select(n => n.name).ToList();
            levelManagerIndex = tempNames.Count == 0 ? 0 : levelManagerIndex;


            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Patrol Path");

            if (enemySpawner.patrolPathIndex > tempNames.Count) levelManagerIndex = enemySpawner.patrolPathIndex;

            tempNames.Insert(0, "None");

            levelManagerIndex = EditorGUILayout.Popup(levelManagerIndex, tempNames.Count > 0
                ? tempNames.ToArray()
                : new[] {""});

            levelManagerIndex -= 1;

            EditorGUILayout.EndHorizontal();
            if (levelManagerIndex != enemySpawner.patrolPathIndex)
            {
                enemySpawner.patrolPathIndex = levelManagerIndex;
                EditorUtility.SetDirty(enemySpawner);
            }
        }

        private void OnSceneGUI()
        {
            //Tools.current = Tool.None;

            var enemySpawner = target as EnemySpawner;
            if (enemySpawner.spawnPoints == null) enemySpawner.spawnPoints = new List<Vector3>();
            if (spwnPoints)
            {
                for (int i = 0; i < enemySpawner.spawnPoints.Count; i++)
                {
                    EditorGUI.BeginChangeCheck();
                    Vector3 newTargetPosition =
                        Handles.PositionHandle(enemySpawner.spawnPoints[i], Quaternion.identity);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(enemySpawner, "Change Spawn Point Position");
                        enemySpawner.spawnPoints[i] = newTargetPosition;
                        EditorUtility.SetDirty(target);
                    }
                }
            }
        }
    }
}