﻿using UnityEngine;
using UnityEngine.VFX;

namespace Puzzle.Trigger
{
    public class Torch : Trigger
    {
        [SerializeField] public Light light;
        [SerializeField] public VisualEffect vfx;

        protected override void Activate()
        {
            light.enabled = true;
            vfx.Play();
        }

        protected override void Deactivate()
        {
            light.enabled = false;
            vfx.Stop();
        }
    }
}