﻿using System;
using UnityEngine;

namespace Puzzle.Trigger
{
    public abstract class Trigger : MonoBehaviour
    {
        // the object that determines what state this is in
        [SerializeField] public Switch switchObject;

        protected virtual void OnEnable()
        {
            switchObject.state.stateChanged += StateChanged;
        }

        protected virtual void OnDisable()
        {
            switchObject.state.stateChanged -= StateChanged;
        }

        // whenever the switchObject state changes, this is called to either activate or deactivate this object
        private void StateChanged(bool state)
        {
            if (switchObject.state.State) Activate();
            else Deactivate();
        }

        /// <summary>
        /// Activate the trigger object
        /// </summary>
        protected abstract void Activate();

        /// <summary>
        /// Deactivate the trigger object
        /// </summary>
        protected abstract void Deactivate();
    }
}