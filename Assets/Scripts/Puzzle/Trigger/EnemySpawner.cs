﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Character;
using Controller;
using Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Puzzle.Trigger
{
    public class EnemySpawner : Trigger
    {
        public int NumberOfEnemies;
        public EnemyCharacter EnemyPrefab;


        public GameObject ShowEnemySpawnObject;
        public Vector2 AmountToShowEnemySpawn = Vector2.zero;


        public Vector2 SpawnDelay;

        public List<Vector3> spawnPoints;

        private List<EnemyCharacter> currentEnemies = new List<EnemyCharacter>();
        private bool active;
        private LevelManager levelManager;
        private bool currentLevel;
        private bool _isShowEnemySpawnObjectNotNull;
        public int patrolPathIndex;

        private void Start()
        {
            _isShowEnemySpawnObjectNotNull = ShowEnemySpawnObject != null;
            levelManager =
                GameManager.gameManager.levelMangerList.FirstOrDefault(l => l.LevelName == gameObject.scene.name);
        }

        protected override void Activate()
        {
            active = true;
            if (currentLevel == false) return;
            var numToSpawn = NumberOfEnemies - currentEnemies.Count;
            for (int i = 0; i < numToSpawn; i++)
            {
                StartCoroutine(SpawnEnemy());
            }
        }

        private void Update()
        {
            if (levelManager == null)
                levelManager =
                    GameManager.gameManager.levelMangerList.FirstOrDefault(l => l.LevelName == gameObject.scene.name);
            if (currentLevel == false && levelManager == LevelManager.levelManager)
            {
                //level is the current level
                currentLevel = true;
                if (active) Activate();
            }
        }

        private void Died(Character.Character character)
        {
            if (character is EnemyCharacter enemy && currentEnemies.Contains(enemy))
            {
                enemy.Died -= Died;
                currentEnemies.Remove(enemy);
                if (currentEnemies.Count < NumberOfEnemies) StartCoroutine(SpawnEnemy());
            }
        }

        private IEnumerator SpawnEnemy()
        {
            var totalDelay = Random.Range(SpawnDelay.x, SpawnDelay.y);
            //if the spawn points are empty use the location of the spawner
            var showEnemySpawnDelay = Random.Range(AmountToShowEnemySpawn.x, AmountToShowEnemySpawn.y);
            var enemySpawnPosition = spawnPoints.Count == 0
                ? transform.position
                : spawnPoints[Random.Range(0, spawnPoints.Count)];

            yield return new WaitForSeconds(totalDelay - showEnemySpawnDelay);
            //show enemy spawn game object
            if (_isShowEnemySpawnObjectNotNull)
            {
                var showEnemySpawn = Instantiate(ShowEnemySpawnObject, enemySpawnPosition, Quaternion.identity);

                //wait for designated time
                yield return new WaitForSeconds(showEnemySpawnDelay);
                //remove the object
                Destroy(showEnemySpawn);
            }

            do
            {
                if (active && currentEnemies.Count < NumberOfEnemies)
                {
                    if (!currentEnemies.Any(e => Vector3.Distance(e.transform.position, enemySpawnPosition) < 0.5f))
                    {
                        var enemy = Instantiate(EnemyPrefab, enemySpawnPosition, Quaternion.identity);
                        var controller = enemy.GetComponent<EnemyController>();
                        controller.thisLevelManager = levelManager;
                        controller.patrolPathIndex = patrolPathIndex;
                        currentEnemies.Add(enemy);
                        enemy.Died += Died;
                        break;
                    }
                }
                else break;

                yield return new WaitForSeconds(0.5f);
            } while (true);
        }

        protected override void Deactivate()
        {
            active = false;
        }


        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            foreach (var spawnPoint in spawnPoints)
            {
                Gizmos.DrawSphere(spawnPoint, 0.1f);
            }
        }
    }
}