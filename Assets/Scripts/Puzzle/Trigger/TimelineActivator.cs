﻿using UnityEngine;
using UnityEngine.Playables;

namespace Puzzle.Trigger
{
    public class TimelineActivator : Trigger
    {
        public PlayableDirector playable;
        private bool notActivated;


        protected override void Activate()
        {
            if (!notActivated)
            {
                notActivated = true;
                playable.Play();
            }
        }

        protected override void Deactivate()
        {
            
        }
    }
}