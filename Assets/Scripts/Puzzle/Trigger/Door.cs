﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Character;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Puzzle.Trigger
{
    public class Door : Trigger
    {
        [SerializeField] private Animator animator;

        [SerializeField] private bool SetNextSceneCurrent;
        
        private static readonly int Open = Animator.StringToHash("Open");

        protected override void Activate()
        {
            animator.SetBool(Open, true);

            if (SetNextSceneCurrent)
            {
                var index = GameManager.gameManager.levelMangerList.FindIndex(l =>
                    l.LevelName == gameObject.scene.name);
                index += 1;

                Debug.Log(index);

                var levelManager = GameManager.gameManager.levelMangerList[index];

                if (levelManager == null)
                {
                    Debug.LogError("Levelmanager not found");
                }
                else
                {
                    levelManager.SetCurrentLevel(false);
                    SceneManager.SetActiveScene(levelManager.gameObject.scene);
                }
            }
        }

        protected override void Deactivate()
        {
            animator.SetBool(Open, false);
        }
    }
}