﻿using Character;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorFence : MonoBehaviour
{

    private List<PlayerCharacter> players;

    void Awake()
    {
        players = new List<PlayerCharacter>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerCharacter>() is PlayerCharacter player)
        {
            players.Add(player);
            if (player is Reaper reaper)
            {
                //todo show attack button
            }
            else if (player is Soul soul)
            {
                soul.EnableReaperLegoHead();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerCharacter>() is PlayerCharacter player && players.Contains(player))
        {
            players.Remove(player);
            if (player is Reaper reaper)
            {
                //todo hide attack button
            }
            else if (player is Soul soul)
            {
                soul.DisableReaperLegoHead();
            }
        }
    }
}
