﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Puzzle.Trigger
{
    public class RNGTrigger : Trigger
    {
        public List<ProxySwitch> ProxySwitches;

        public ActivatableDial DialSwitch;

        private int number;

        protected override void Activate()
        {
            if (ProxySwitches != null && ProxySwitches.Count >= 1)
            {
                number = Random.Range(1, ProxySwitches.Count);
                for (int i = 0; i < ProxySwitches.Count; i++)
                {
                    ProxySwitches[i].state.State = (i <= number);
                }


                DialSwitch.states = new bool[ProxySwitches.Count].ToList();
                DialSwitch.states[number] = true;
                DialSwitch.currentIndex = 0;
                DialSwitch.Activate();
            }
            else
            {
                Debug.LogError("No proxy switches found", this);
            }
        }

        protected override void Deactivate()
        {
            for (int i = 0; i < (ProxySwitches?.Count ?? 0); i++)
            {
                if (ProxySwitches[i].state.State == true)
                    ProxySwitches[i].state.State = false;
            }

            DialSwitch.DeActivate();
        }
    }
}