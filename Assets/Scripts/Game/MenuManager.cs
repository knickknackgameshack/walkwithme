﻿using System.Linq;
using System.Collections.Generic;
using System.IO.Compression;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using Input = Controller.Input;
using UnityEngine.UI;
using UI;

namespace Game
{
    public class MenuManager : MonoBehaviour
    {
        [SerializeField] private Toggle fsToggle;
        private bool isFullScreen = true;
        private Resolution[] resolutions;
        public List<Canvas> menus;

        [SerializeField] private TMPro.TMP_Dropdown dropdown;
        [SerializeField] public InputSystemUIInputModule input;
        private bool twoControllers;
        private bool assigned;

        [SerializeField] private TextMeshProUGUI versionBox;

        private void Awake()
        {
            fsToggle.SetIsOnWithoutNotify(Screen.fullScreen);

            GameManager.Input.menuControls = input.actionsAsset;

            resolutions = Screen.resolutions;
            dropdown.ClearOptions();

            var resList = resolutions.Distinct().ToList();

            foreach (var res in resList)
                dropdown.options.Add(new TMPro.TMP_Dropdown.OptionData() { text = res.width + ":" + res.height });


            var index = dropdown.options.FindIndex(r =>
                r.text == $"{Screen.currentResolution.width}:{Screen.currentResolution.height}");

            if (index == -1)
            {
                dropdown.options.Insert(0,
                    new TMPro.TMP_Dropdown.OptionData()
                    { text = $"{Screen.currentResolution.width}:{Screen.currentResolution.height}" });
                index = 0;
            }

            dropdown.SetValueWithoutNotify(index);

            if (GameManager.Input != null)
            {
                //get list of supported types
                var controlModes = GameManager.Input.GetControlModes();

                if (controlModes.Contains(Input.ControlMode.Default))
                {
                    twoControllers = true;
                }
                else if (controlModes.Contains(Input.ControlMode.SingleController))
                {
                    GameManager.Input.SetupControls(Input.ControlMode.SingleController);
                    assigned = true;
                }
                else
                {
                    GameManager.Input.SetupControls(Input.ControlMode.Keyboard);
                    assigned = true;
                }
            }

            versionBox.text = $"Version : {Application.version}";
        }

        private void Start()
        {
            AudioManager.Instance.MenuSource.Play();
        }

        public void PlayGame()
        {
            if (GameManager.Input != null)
            {
                if (!assigned)
                {
                    var soul = Gamepad.all.Where(g => g != Gamepad.current).First();
                    GameManager.Input.AssignChar(Gamepad.current, soul);
                    assigned = true;
                }
                StartCoroutine(AudioManager.Instance.FadeOutMusic(AudioManager.Instance.MenuSource, 1f));
                StartCoroutine(AudioManager.Instance.FadeInMusic(AudioManager.Instance.AmbientSource, 1f));
                StartCoroutine(GameManager.gameManager.DualFade(1f, GameManager.gameManager.levels.First()));
                //StartCoroutine(GameManager.gameManager.FadeToBlack(1f, true));
                //GameManager.gameManager.SwitchToLevel(GameManager.gameManager.levels.First());
                AudioManager.Instance.PlayAmbient();
            }
        }

        public void PlayLevel(string levelName)
        {
            if (GameManager.Input != null)
                StartCoroutine(GameManager.gameManager.DualFade(1f, levelName));
            //GameManager.gameManager.SwitchToLevel(levelName);
        }

        public void SetReaperControlMode(int value)
        {
            GameManager.gameManager.preferences.ReaperControlMode = (Input.ControlMode)value;
        }

        public void SetSoulControlMode(int value)
        {
            GameManager.gameManager.preferences.SoulControlMode = (Input.ControlMode)value;
        }

        public void AssignReaper()
        {
            if (twoControllers == true)
            {
                var soul = Gamepad.all.Where(g => g != Gamepad.current).First();
                GameManager.Input.AssignChar(Gamepad.current, soul);
                assigned = true;
            }
        }

        public void AssignSoul()
        {
            if (twoControllers == true)
            {
                var reaper = Gamepad.all.Where(g => g != Gamepad.current).First();
                GameManager.Input.AssignChar(reaper, Gamepad.current);
                assigned = true;
            }
        }

        public void GoToMenu(Canvas thisCanvas)
        {
            for (int i = 0;
                i < menus.Count();
                i++)
            {
                menus[i].gameObject.SetActive(false);
            }

            thisCanvas.gameObject.SetActive(true);
            thisCanvas.gameObject.GetComponent<SetButtonActive>().SetButton();
        }

        public void SetResolution(int value)
        {
            Screen.SetResolution(resolutions[value].width, resolutions[value].height, isFullScreen);
        }

        public void IsFullScreen(bool fullscreen)
        {
            if (fullscreen == true)
            {
                Screen.fullScreen = true;
                isFullScreen = true;
            }

            else
            {
                Screen.fullScreen = false;
                isFullScreen = false;
            }
        }
    }
}