﻿using System;
using System.Collections.Generic;
using Character;
using OdinSerializer;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class LevelManager : MonoBehaviour
    {
        public string LevelName;

        [SerializeField] public List<PatrolPoints> patrolPoints;

        public static LevelManager levelManager = null;

        public Vector3 SoulSpawn, ReaperSpawn;


        private void Awake()
        {
            if (levelManager == null)
            {
                SetCurrentLevel(true);
            }
            else
            {
                GameManager.gameManager.levelMangerList.Add(this);
            }
        }


        public void SetCurrentLevel(bool restart)
        {
            if (!GameManager.gameManager.levelMangerList.Contains(this))
                GameManager.gameManager.levelMangerList.Add(this);

            levelManager = this;
            GameManager.gameManager.cameraController.gameObject.SetActive(true);
            GameManager.gameManager.Reaper.gameObject.SetActive(true);
            GameManager.gameManager.Soul.gameObject.SetActive(true);
            if (restart)
            {
                MovePlayers(ref GameManager.gameManager.Reaper, ref GameManager.gameManager.Soul);
                GameManager.Input.EnableControls();
            }

            SceneManager.SetActiveScene(gameObject.scene);
        }

        public void MovePlayers(ref Reaper reaper, ref Soul soul)
        {
            reaper.Rigidbody.position = ReaperSpawn;
            soul.Rigidbody.position = SoulSpawn;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(SoulSpawn, 0.4f);

            Gizmos.color = Color.black;
            Gizmos.DrawSphere(ReaperSpawn, 0.4f);

            if (patrolPoints == null) return;

            int numPerHue = 15;

            for (int i = 0; i < patrolPoints.Count; i++)
            {
                float hue = (float) (i % (numPerHue + 1)) / numPerHue;
                float satval = 1 - Mathf.Floor((float) i / numPerHue) / 4;

                var color = Color.HSVToRGB(hue, satval, satval);

                Gizmos.color = color;
                foreach (var point in patrolPoints[i].points)
                {
                    Gizmos.DrawSphere(point, 0.3f);
                }
            }
        }
    }


    [Serializable]
    public class PatrolPoints
    {
        [SerializeField] public string name;

        [SerializeField] public List<Vector3> points;

        public void SetName(string newName)
        {
            name = newName;
        }

        /// <summary>
        /// Get closest point on patrol path to the supplied position
        /// </summary>
        /// <param name="point">Position that we are finding the closest point to</param>
        /// <returns>The index of the position of the closest point</returns>
        public int GetClosestPoint(Vector3 point)
        {
            var closest = 0;
            for (var i = 1; i < points.Count; i++)
            {
                if (Vector3.Distance(points[i], point) < Vector3.Distance(points[closest], point))
                {
                    closest = i;
                }
            }

            return closest;
        }
    }
}