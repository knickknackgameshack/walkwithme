﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private Slider slider;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Image image;

        public float Progress
        {
            set
            {
                var percent = Mathf.Clamp(value, 0, 1);

                if (slider != null)
                {
                    slider.value = percent;
                }

                if (text != null)
                {
                    text.text = (percent * 100) + "%";
                }

                if (image != null)
                {
                    image.fillAmount = percent;
                }
            }
        }
    }
}