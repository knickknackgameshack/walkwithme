﻿using UnityEditor;
using UnityEngine;

namespace Game
{
    [CustomEditor(typeof(FogFixer))]
    public class FogFixerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var fog = target as FogFixer;

            if (GUILayout.Button("Fix Fog"))
            {
                fog.FixFog();
            }
        }
    }
}