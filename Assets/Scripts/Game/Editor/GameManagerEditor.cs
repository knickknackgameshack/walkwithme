﻿using System.Collections.Generic;
using Character;
using Controller;
using Narrative;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Input = Controller.Input;

namespace Game
{
    [CustomEditor(typeof(GameManager))]
    public class GameManagerEditor : Editor
    {
        private bool preferences;
        private bool textDictionaries;
        private bool characters;
        private bool misc;
        private bool scenes;
        private bool levels;
        private bool fonts;

        public override void OnInspectorGUI()
        {
            //styles
            GUIStyle redFont = new GUIStyle(GUI.skin.label)
                {normal = {textColor = Color.red * 0.6f}, fontStyle = FontStyle.Bold};
            GUIStyle greenFont = new GUIStyle(GUI.skin.label)
                {normal = {textColor = Color.green * 0.6f}, fontStyle = FontStyle.Bold};


            var gameManagerObj = target as GameManager;

            EditorGUILayout.LabelField("Current State");
            var state = (State) EditorGUILayout.EnumPopup(gameManagerObj.State);
            if (state != gameManagerObj.State)
            {
                gameManagerObj.State = state;
                EditorUtility.SetDirty(gameManagerObj);
            }


            EditorGUILayout.Space();
            scenes = EditorGUILayout.Foldout(scenes, "Scenes");
            if (scenes)
            {
                var menu = EditorGUILayout.TextField("Menu Scene", gameManagerObj.menu);
                if (menu != gameManagerObj.menu)
                {
                    gameManagerObj.menu = menu;
                    EditorUtility.SetDirty(gameManagerObj);
                }

                levels = EditorGUILayout.Foldout(levels, "Levels");
                if (levels)
                {
                    if (gameManagerObj.levels == null)
                    {
                        gameManagerObj.levels = new List<string>();
                        EditorUtility.SetDirty(gameManagerObj);
                    }

                    for (int i = 0; i < gameManagerObj.levels.Count; i++)
                    {
                        EditorGUILayout.BeginHorizontal();
                        var name = EditorGUILayout.TextField("Level " + (i + 1) + " Scene", gameManagerObj.levels[i]);
                        if (name != gameManagerObj.levels[i])
                        {
                            gameManagerObj.levels[i] = name;
                            EditorUtility.SetDirty(gameManagerObj);
                        }


                        if (GUILayout.Button("-"))
                        {
                            gameManagerObj.levels.RemoveAt(i);
                            EditorUtility.SetDirty(gameManagerObj);
                            break;
                        }

                        EditorGUILayout.EndHorizontal();
                    }

                    if (GUILayout.Button("Add Level"))
                    {
                        gameManagerObj.levels.Add("");
                        EditorUtility.SetDirty(gameManagerObj);
                    }
                }
            }


            EditorGUILayout.Space();

            preferences = EditorGUILayout.Foldout(preferences, "Preferences");
            if (preferences)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Game Language");
                var tempGameLanguage = (Language) EditorGUILayout.EnumPopup(gameManagerObj.preferences.gameLanguage);
                if (gameManagerObj.preferences.gameLanguage != tempGameLanguage)
                {
                    gameManagerObj.preferences.gameLanguage = tempGameLanguage;
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Reaper Control Mode");
                var tempReaperControlMode =
                    (Input.ControlMode) EditorGUILayout.EnumPopup(gameManagerObj.preferences.ReaperControlMode);
                if (gameManagerObj.preferences.reaperControlMode != tempReaperControlMode)
                {
                    gameManagerObj.preferences.ReaperControlMode = tempReaperControlMode;
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Soul Control Mode");
                var tempSoulControlMode =
                    (Input.ControlMode) EditorGUILayout.EnumPopup(gameManagerObj.preferences.SoulControlMode);
                if (gameManagerObj.preferences.soulControlMode != tempSoulControlMode)
                {
                    gameManagerObj.preferences.SoulControlMode = tempSoulControlMode;
                }

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();

            var soul = FindObjectOfType<Soul>();
            var reaper = FindObjectOfType<Reaper>();
            var soulController = FindObjectOfType<SoulController>();
            var reaperController = FindObjectOfType<ReaperController>();
            var cameraController = FindObjectOfType<CameraController>();


            characters = EditorGUILayout.Foldout(characters, "Found Characters");
            if (characters)
            {
                //reaper
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Reaper found: ");
                if (reaper != null) EditorGUILayout.LabelField("true", greenFont);
                else EditorGUILayout.LabelField("false", redFont);
                EditorGUILayout.EndHorizontal();

                //soul
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Soul found: ");
                if (soul != null) EditorGUILayout.LabelField("true", greenFont);
                else EditorGUILayout.LabelField("false", redFont);
                EditorGUILayout.EndHorizontal();

                //Camera Controller
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Camera Controller Found: ");
                if (cameraController != null) EditorGUILayout.LabelField("true", greenFont);
                else EditorGUILayout.LabelField("false", redFont);
                EditorGUILayout.EndHorizontal();


                //Character Controller
                //reaper
                if (reaperController != null && reaperController.Reaper != reaper)
                {
                    reaperController.Reaper = reaper;
                    EditorUtility.SetDirty(reaperController);
                }

                //soul
                if (soulController != null && soulController.Soul != soul)
                {
                    soulController.Soul = soul;
                    EditorUtility.SetDirty(soulController);
                }

                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Character Controllers Configured: ");
                if (soulController != null && reaperController != null && soulController.Soul != null &&
                    reaperController.Reaper != null)
                    EditorGUILayout.LabelField("true", greenFont);
                else EditorGUILayout.LabelField("false", redFont);
                EditorGUILayout.EndHorizontal();
            }

            if (gameManagerObj.Reaper != reaper && reaper != null)
            {
                gameManagerObj.Reaper = reaper;
                EditorUtility.SetDirty(gameManagerObj);
            }

            if (gameManagerObj.Soul != soul && soul != null)
            {
                gameManagerObj.Soul = soul;
                EditorUtility.SetDirty(gameManagerObj);
            }

            if (gameManagerObj.ReaperController != reaperController && reaperController != null)
            {
                gameManagerObj.ReaperController = reaperController;
                EditorUtility.SetDirty(gameManagerObj);
            }

            if (gameManagerObj.SoulController != soulController && soulController != null)
            {
                gameManagerObj.SoulController = soulController;
                EditorUtility.SetDirty(gameManagerObj);
            }

            if (cameraController != null && gameManagerObj.cameraController != cameraController)
            {
                gameManagerObj.cameraController = cameraController;
                EditorUtility.SetDirty(gameManagerObj);
            }

            EditorGUILayout.Space();

            textDictionaries = EditorGUILayout.Foldout(textDictionaries, "Found Text Dictionaries");

            if (textDictionaries)
            {
                foreach (var textDictionary in GameManager.GetTextDictionaries())
                {
                    EditorGUILayout.LabelField(textDictionary.name);
                }
            }

            EditorGUILayout.Space();

            misc = EditorGUILayout.Foldout(misc, "Misc");

            if (misc)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Loading Screen:");
                var loadingScreen = FindObjectOfType<LoadingScreen>();
                if (loadingScreen != null) EditorGUILayout.LabelField("true", greenFont);
                else EditorGUILayout.LabelField("false", redFont);
                if (loadingScreen != null)
                {
                    gameManagerObj.loadingScreen = loadingScreen;
                    EditorUtility.SetDirty(gameManagerObj);
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                var transitionText = EditorGUILayout.ObjectField("Level Transition Text", gameManagerObj.transitionText,
                    typeof(TMPro.TextMeshProUGUI), true);
                if (transitionText != gameManagerObj.transitionText)
                {
                    gameManagerObj.transitionText = (TMPro.TextMeshProUGUI) transitionText;
                    EditorUtility.SetDirty(gameManagerObj);
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                var blackImage = (Image) EditorGUILayout.ObjectField("Black Screen", gameManagerObj.blackScreen,
                    typeof(Image), true);
                if (blackImage != gameManagerObj.blackScreen)
                {
                    gameManagerObj.blackScreen = (Image) blackImage;
                    EditorUtility.SetDirty(gameManagerObj);
                }

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            fonts = EditorGUILayout.Foldout(fonts, "Fonts");
            if (fonts)
            {
                EditorGUILayout.BeginHorizontal();
                var reaperFont = (TMP_FontAsset) EditorGUILayout.ObjectField("Reaper Font", gameManagerObj.ReaperFont,
                    typeof(TMP_FontAsset), true);
                if (reaperFont != gameManagerObj.ReaperFont)
                {
                    gameManagerObj.ReaperFont = reaperFont;
                    EditorUtility.SetDirty(gameManagerObj);
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                var soulFont = (TMP_FontAsset) EditorGUILayout.ObjectField("Soul Font", gameManagerObj.SoulFont,
                    typeof(TMP_FontAsset), true);
                if (soulFont != gameManagerObj.SoulFont)
                {
                    gameManagerObj.SoulFont = soulFont;
                    EditorUtility.SetDirty(gameManagerObj);
                }

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}