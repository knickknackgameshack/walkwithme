﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Game
{
    public class CreateLevelScene
    {
        [MenuItem("Assets/Knick Knack Game Shack/Create/Level"), MenuItem("Knick Knack Game Shack/Create/Level")]
        private static void CreateNewSceneForLevel()
        {
            var newLevel = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
            newLevel.name = "New Level";
            EditorUtility.FocusProjectWindow();
//            EditorSceneManager.OpenScene(newLevel.name);

            var levelManager = new GameObject {name = "LevelManager"};
            var level = levelManager.AddComponent<LevelManager>();

            //todo more level setup stuff

            EditorSceneManager.SaveScene(newLevel, "Assets/Scenes/" + newLevel.name + ".unity");
        }
    }
}