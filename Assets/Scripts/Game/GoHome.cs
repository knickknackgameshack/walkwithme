﻿using UnityEngine;

namespace Game
{
    public class GoHome : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            //Game.GameManager.gameManager.UnloadScene("L1-S-A");
            // Game.GameManager.gameManager.UnloadScene("L2-S-A");
            //Game.GameManager.gameManager.UnloadScene("L3");
            //Game.GameManager.gameManager.UnloadScene("L4-S");
            Game.GameManager.gameManager.GoToMenu();
        }
    }
}
