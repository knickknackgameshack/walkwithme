﻿using UnityEngine;

namespace Game
{
    public class FogFixer : MonoBehaviour
    {
        [SerializeField] private Material fog;
        [SerializeField] private Texture3D fogNoise;
        private static readonly int VolumetricNoiseTexture = Shader.PropertyToID("_VolumetricNoiseTexture");


        private void Awake()
        {
           FixFog();
        }

        public void FixFog()
        {
            if (fog != null && fogNoise != null)
            {
                fog.SetTexture(VolumetricNoiseTexture, fogNoise);
            }
        }
    }
}