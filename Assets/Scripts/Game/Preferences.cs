﻿using System;
using System.Runtime.CompilerServices;
using Narrative;
using UnityEngine;
using Input = Controller.Input;

namespace Game
{
    [Serializable]
    public struct Preferences
    {
        [SerializeField] public Input.ControlMode reaperControlMode;
        [SerializeField] public Input.ControlMode soulControlMode;

        [SerializeField] public Language gameLanguage;

        public Input.ControlMode ReaperControlMode
        {
            get { return reaperControlMode; }
            set { reaperControlMode = value; }
        }

        public Input.ControlMode SoulControlMode
        {
            get { return soulControlMode; }
            set { soulControlMode = value; }
        }
    }
}