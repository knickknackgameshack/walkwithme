﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Character;
using Controller;
using Narrative;
using OdinSerializer.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Input = Controller.Input;

namespace Game
{
    public enum State
    {
        Menu,
        Combat,
        InGame,
        Pause
    }

    public class GameManager : MonoBehaviour
    {
        public static GameManager gameManager;

        public TMPro.TextMeshProUGUI transitionText;
        public Image blackScreen;

        public State State;

        [SerializeField] public LoadingScreen loadingScreen;

        [SerializeField] public Reaper Reaper;
        [SerializeField] public Soul Soul;

        public ReaperController ReaperController;
        public SoulController SoulController;
        public CameraController cameraController;

        public Action<bool> Pause;

        private static Language gameLanguage;
        private static Dictionary<string, TextDictionary> textDictionaries;
        [SerializeField] public Preferences preferences;


        public string menu;
        public List<string> levels;
        private LevelManager currentLevel;
        public static Input Input;
        public InputDevice Player1;

        private bool fadeOff;
        public TMP_FontAsset ReaperFont;
        public TMP_FontAsset SoulFont;
        public List<LevelManager> levelMangerList = new List<LevelManager>();

        private void Awake()
        {
            gameManager = this;
            textDictionaries = new Dictionary<string, TextDictionary>();
            var assets = GetTextDictionaries();
            foreach (var dialogue in assets)
            {
                textDictionaries.Add(dialogue.name, dialogue);
            }


            Input = FindObjectOfType<Input>();
            Soul = FindObjectOfType<Soul>();
            Reaper = FindObjectOfType<Reaper>();
            cameraController = FindObjectOfType<CameraController>();
            SoulController = FindObjectOfType<SoulController>();
            ReaperController = FindObjectOfType<ReaperController>();
            loadingScreen = FindObjectOfType<LoadingScreen>();
            fadeOff = false;
        }

        private void OnEnable()
        {
            Input.Pause += PauseInput;
        }

        private void OnDisable()
        {
            Input.Pause -= PauseInput;
        }

        private void PauseInput() //todo show menu
        {
            if (State == State.InGame)
            {
                State = State.Pause;
                Pause?.Invoke(true);
            }
            else if (State == State.Pause)
            {
                State = State.InGame;
                Pause?.Invoke(false);
            }
        }

        private void Start()
        {
            InitMenu();
        }

        private void InitMenu()
        {
            cameraController.gameObject.SetActive(false);
            Reaper.gameObject.SetActive(false);
            Soul.gameObject.SetActive(false);
            transitionText.gameObject.SetActive(false);
            StartCoroutine(LoadLevel(menu));
        }

        public static List<TextDictionary> GetTextDictionaries()
        {
            var dicts = Resources.LoadAll<TextDictionary>("Narrative").ToList();
            return dicts;
        }


        public void SwitchToLevel(int levelIndex)
        {
        }

        public void GoToMenu()
        {
            loadingScreen.Progress = 0;
            loadingScreen.gameObject.SetActive(true);

            var scenes = SceneManager.sceneCount;

            for (int i = scenes - 1; i >= 0; i--)
            {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.name != "Persistant")
                {
                    UnloadScene(scene.name);
                }
            }

            InitMenu();
        }

        public void LoadNextScene(string s)
        {
            StartCoroutine(LoadLevelNoLoadingScreen(s));
            StartCoroutine(ShowAndFadeLevelName(s));
        }


        private IEnumerator ShowAndFadeLevelName(string s)
        {
            transitionText.gameObject.SetActive(true);
            transitionText.color = Color.white;
            float lerp = 0.1f;
            do
            {
                yield return new WaitForSeconds(lerp);
                transitionText.color = Vector4.Lerp(Color.white, Color.clear, lerp);
            } while (transitionText.color == Color.clear);

            transitionText.gameObject.SetActive(false);
        }

        public void UnloadScene(string s)
        {
            StartCoroutine(UnLoadLevel(s));
        }

        public void SwitchToLevel(string levelName)
        {
            StartCoroutine(UnLoadLevel(SceneManager.GetActiveScene().name, LoadLevel(levelName)));
        }


        private IEnumerator LoadLevel(string sceneName)
        {
            loadingScreen.gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();
            var asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            asyncOperation.allowSceneActivation = false;
            while (!asyncOperation.isDone)
            {
                loadingScreen.Progress = asyncOperation.progress;
                if (asyncOperation.progress >= 0.9f) asyncOperation.allowSceneActivation = true;
                yield return null;
            }

            var scene = SceneManager.GetSceneByName(sceneName);
            SceneManager.SetActiveScene(scene);

            if (sceneName != menu && !sceneName.IsNullOrWhitespace())
            {
                State = State.InGame;
            }


            loadingScreen.gameObject.SetActive(false);
            if (fadeOff == true)
                yield return StartCoroutine(FadeToClear(6f));
        }

        private IEnumerator LoadLevelNoLoadingScreen(string sceneName)
        {
            yield return new WaitForEndOfFrame();
            var asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            // asyncOperation.allowSceneActivation = false;
            yield return asyncOperation;

            if (sceneName != menu && !sceneName.IsNullOrWhitespace())
            {
                State = State.InGame;
            }
        }

        private IEnumerator UnLoadLevel(string unloadSceneName, IEnumerator loadScene)
        {
            //loadingScreen.gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();

            var asyncOperation = SceneManager.UnloadSceneAsync(unloadSceneName);
            while (!asyncOperation.isDone) yield return null;
            StartCoroutine(loadScene);
        }


        private IEnumerator UnLoadLevel(string unloadSceneName)
        {
            yield return new WaitForEndOfFrame();
            var asyncOperation = SceneManager.UnloadSceneAsync(unloadSceneName);
            while (!asyncOperation.isDone) yield return null;
        }

        public static string GetDialogue(string textDictionaryName, string textDictionaryKey)
        {
            return textDictionaries[textDictionaryName].Text[textDictionaryKey][gameLanguage];
        }

        public static void GameOver(GameResult result)
        {
            switch (result)
            {
                case GameResult.SoulDied:
                    //todo let the players know
                    //gameManager.ResetCharacters();
                    //gameManager.SwitchToLevel(SceneManager.GetActiveScene().name);
                    gameManager.levelMangerList.Clear();
                    gameManager.GoToMenu();

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(result), result, null);
            }
        }

        private void ResetCharacters()
        {
            Reaper.Health = 100;
            Soul.Health = 100;
        }


        public IEnumerator DualFade(float fadeTime, string sceneName)
        {
            blackScreen.gameObject.SetActive(true);
            blackScreen.color = Color.clear;
            float rate = 1.0f / fadeTime;
            float progress = 0.0f;
            while (progress < 1.0)
            {
                blackScreen.color = Color.Lerp(Color.clear, Color.black, progress);
                progress += rate * Time.deltaTime;
                yield return null;
            }

            blackScreen.color = Color.black;
            fadeOff = true;

            yield return StartCoroutine(UnLoadLevel(SceneManager.GetActiveScene().name, LoadLevel(sceneName)));

            yield return StartCoroutine(FadeToClear(fadeTime));

            /*blackScreen.gameObject.SetActive(true);
            blackScreen.color = Color.black;
            float rate2 = 1.0f / fadeTime;
            float progress2 = 0.0f;
            while (progress2 < 1.0)
            {
                blackScreen.color = Color.Lerp(Color.black, Color.clear, progress2);
                progress2 += rate2 * Time.deltaTime;
                yield return null;
            }
            blackScreen.color = Color.clear;
            blackScreen.gameObject.SetActive(false);*/
        }

        public IEnumerator FadeToClear(float fadeTime)
        {
            blackScreen.gameObject.SetActive(true);
            blackScreen.color = Color.black;
            float rate = 1.0f / fadeTime;
            float progress = 0.0f;
            while (progress < 1.0)
            {
                blackScreen.color = Color.Lerp(Color.black, Color.clear, progress);
                progress += rate * Time.deltaTime;
                yield return null;
            }

            blackScreen.color = Color.clear;
            blackScreen.gameObject.SetActive(false);
        }

        public IEnumerator FadeToBlack(float fadeTime, bool fromMenu)
        {
            blackScreen.gameObject.SetActive(true);
            blackScreen.color = Color.clear;
            float rate = 1.0f / fadeTime;
            float progress = 0.0f;
            while (progress < 1.0)
            {
                blackScreen.color = Color.Lerp(Color.clear, Color.black, progress);
                progress += rate * Time.deltaTime;
                yield return null;
            }

            blackScreen.color = Color.black;
            blackScreen.gameObject.SetActive(false);
        }
    }

    public enum GameResult
    {
        SoulDied
    }

    public enum Layer
    {
        Player = 10 | Soul | Reaper,
        Soul = 21,
        Reaper = 22,
        Enemy = 11,
        Breakable = 18,
        Puzzle = 16,
        Narrative = 17,
        Floor = 24,
        Wall = 25,
        EnvironmentProps = 26
    }
}