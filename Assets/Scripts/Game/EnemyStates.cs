﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Game
{
    public class EnemyStates : MonoBehaviour
    {
        public List<Controller.EnemyController> enemies;

        public static EnemyStates EnemyStatesRef;
        public float fightTime = 0;
        public float CombatToAmbientDelay = 4;

        private void Awake()
        {
            EnemyStatesRef = this;
        }

        private void FixedUpdate()
        {
            var fight = enemies.Any(e => e.fsm.CurrentState == Controller.State.TrackingReaper ||
                                         e.fsm.CurrentState == Controller.State.TrackingSoul ||
                                         e.fsm.CurrentState == Controller.State.Attack);

            if (fight) fightTime = CombatToAmbientDelay;
            else fightTime -= Time.fixedDeltaTime;

            if (GameManager.gameManager.State == State.InGame || GameManager.gameManager.State == State.Combat)
            {
                if (fightTime > 0) GameManager.gameManager.State = State.Combat;
                else GameManager.gameManager.State = State.InGame;
            }
        }

        public void EnemyDied(Character.Character c)
        {
            if (c is Character.EnemyCharacter enemy &&
                enemies.FirstOrDefault(e => e.enemy == enemy) is Controller.EnemyController controller)
            {
                enemies.Remove(controller);
                enemy.Died -= EnemyDied;
            }
        }
    }
}