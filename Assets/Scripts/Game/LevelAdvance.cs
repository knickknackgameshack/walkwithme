﻿using System.Linq;
using UnityEngine;

namespace Game
{
    public class LevelAdvance : MonoBehaviour
    {
        public enum Status
        {
            LoadNext,
            DeleteThis
        }

        private LevelManager lvl;
        private int nextLevel = -1;

        public Status status;

        public void Start()
        {
            var index = GameManager.gameManager.levels.FindIndex(l => l == gameObject.scene.name);

            nextLevel = index + 1;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player") && this.status == Status.LoadNext)
            {
                GameManager.gameManager.LoadNextScene(GameManager.gameManager.levels[nextLevel]);
                GameManager.gameManager.transitionText.text = GameManager.gameManager.levels[nextLevel];
                Destroy(this.gameObject);
            }
            else if (other.gameObject.CompareTag("Player") && this.status == Status.DeleteThis)
            {
                GameManager.gameManager.UnloadScene(GameManager.gameManager.levels[nextLevel - 1]);
                Destroy(this.gameObject);
            }
        }
    }
}