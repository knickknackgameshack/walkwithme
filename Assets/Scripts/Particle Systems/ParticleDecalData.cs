﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDecalData 
{
    public Vector3 pos;
    public float size;
    public Vector3 rot;
    public Color color;


}
