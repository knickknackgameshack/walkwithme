﻿using Character;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controller
{
    public class ReaperController : PlayerController
    {
        [SerializeField] private Reaper reaper;

        public Reaper Reaper
        {
            get => reaper;
            set => reaper = value;
        }

        private void OnEnable()
        {
            Input.ReaperMove += ReaperMove;
            Input.ReaperHeavyAttack += ReaperHeavyAttack;
            Input.ReaperLightAttack += ReaperLightAttack;
            Input.ReaperInteract += ReaperInteract;
            Input.ReaperEmote += ReaperEmote;
            Input.ReaperSprint += ReaperSprint;
            Input.ReaperParry += ReaperParry;
            Input.ReaperInteractHold += ReaperInteractHold;
        }

        private void ReaperInteractHold(bool obj)
        {
            reaper.InteractHold(obj);
        }

        private void ReaperParry()
        {
            reaper.Parry();
        }

        private void ReaperSprint(bool sprint)
        {
            reaper.Sprint(sprint);
        }

        private void ReaperEmote(PlayerCharacter.EmoteType emote)
        {
            reaper.Emote(emote);
            Debug.Log("Emote: " + emote.ToString());
        }

        private void ReaperLightAttack()
        {
            reaper.LightAttack();
        }

        private void ReaperHeavyAttack()
        {
            reaper.HeavyAttack();
        }

        private void ReaperInteract()
        {
            reaper.Interact();
        }

        private void ReaperMove(Vector2 dir)
        {
            reaper.Move(dir.x * CameraController.Right + dir.y * CameraController.Forward);
        }

        private void OnDisable()
        {
            Input.ReaperMove -= ReaperMove;
            Input.ReaperHeavyAttack -= ReaperHeavyAttack;
            Input.ReaperLightAttack -= ReaperLightAttack;
            Input.ReaperInteract -= ReaperInteract;
            Input.ReaperEmote -= ReaperEmote;
            Input.ReaperSprint -= ReaperSprint;
            Input.ReaperParry -= ReaperParry;
        }
    }
}