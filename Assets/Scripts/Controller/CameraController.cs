﻿using System.ComponentModel;
using Game;
using TMPro;
using UnityEngine;

namespace Controller
{
    public class CameraController : MonoBehaviour
    {
        public static Vector3 Forward;
        public static Vector3 Right;

        [SerializeField] [HideInInspector] public Camera camera;

        [SerializeField] private TextMeshProUGUI debugText;

        private Rigidbody rigidbody;

        private float zoomAmount;

        private Vector3 focusPoint;
        [SerializeField] private float maxZoom = 1;
        [SerializeField] private float minZoom = 10;


        internal static bool UseOverride = false;
        internal static Transform OverrideFocusPoint = null;
        internal static float TargetZoom = 0;

        [SerializeField, Range(0.01f, 5)] private float lerpSpeed;

        [SerializeField, Range(0, 0.25f)] private float innerSafeZone;
        [SerializeField, Range(0.5f, 0.25f)] private float outerSafeZone;
        [SerializeField, ReadOnly(true)] private Vector2 soulView;
        [SerializeField, ReadOnly(true)] private Vector2 reaperView;
        [SerializeField, ReadOnly(true)] private Vector2 cloneView;

        public static Vector3 Position => cameraControllerIsNull ? Vector3.zero : cameraController.transform.position;
        public static float ZoomLevel => cameraControllerIsNull ? -1 : cameraController.camera.orthographicSize;

        private static CameraController cameraController;
        private static bool cameraControllerIsNull;


        private void Awake()
        {
            camera = GetComponentInChildren<Camera>();
            rigidbody = gameObject.GetComponent<Rigidbody>();

            cameraController = this;
            cameraControllerIsNull = false;
        }

        private void FixedUpdate()
        {
            var gm = GameManager.gameManager;

            if (gm.State == Game.State.Pause) return;

            if (UseOverride)
            {
                focusPoint = Vector3.Slerp(focusPoint, OverrideFocusPoint.position, Time.fixedDeltaTime * lerpSpeed);
                rigidbody.MovePosition(focusPoint);

                var orthographicSize = camera.orthographicSize;
                camera.orthographicSize = Mathf.Lerp(orthographicSize,
                    Mathf.Clamp(TargetZoom, maxZoom, minZoom), Time.fixedDeltaTime * lerpSpeed);
            }
            else
            {
                if (gm == null) return;
                //find reaper pos and move direction
                var reaperPos = gm.Reaper.transform.position;
                var reaperForward = gm.Reaper.transform.forward;

                //find soul pos and move direction
                var soulPos = gm.Soul.transform.position;
                var soulForward = gm.Soul.transform.forward;

                var averagePoint = (soulPos + soulForward * 3 + Vector3.up * 2 + reaperPos + reaperForward * 3 +
                                    Vector3.up * 2) / 2;


                //focus point becomes the center of the screen
                focusPoint = Vector3.Slerp(focusPoint, averagePoint, Time.fixedDeltaTime * lerpSpeed);


                rigidbody.MovePosition(focusPoint);


                //since we havent moved the camera yet, either one or both could be out of our safe zone
                reaperView = camera.WorldToViewportPoint(reaperPos + reaperForward * 3 + Vector3.up * 2);
                soulView = camera.WorldToViewportPoint(soulPos + soulForward * 3 + Vector3.up * 2);

                soulView = AbsoluteV2(soulView - (0.5f * Vector2.one));
                reaperView = AbsoluteV2(reaperView - (0.5f * Vector2.one));


//            zoomAmount -=
                var soulAmount = Mathf.Clamp(outerSafeZone - soulView.x, -1, 0) +
                                 Mathf.Clamp(outerSafeZone - soulView.y, -1, 0);
                if (soulAmount >= 0)
                    soulAmount += Mathf.Clamp(innerSafeZone - soulView.x, 0, 1) +
                                  Mathf.Clamp(innerSafeZone - soulView.y, 0, 1);
                var reaperAmount = Mathf.Clamp(outerSafeZone - reaperView.y, -1, 0) +
                                   Mathf.Clamp(outerSafeZone - reaperView.x, -1, 0);
                if (reaperAmount >= 0)
                    reaperAmount += Mathf.Clamp(innerSafeZone - reaperView.x, 0, 1) +
                                    Mathf.Clamp(innerSafeZone - reaperView.y, 0, 1);

                zoomAmount -= reaperAmount + soulAmount;


//            debugText.text = zoomAmount.ToString(CultureInfo.CurrentCulture);
                // if (gm.cloneExists) zoomAmount /= 3;
                zoomAmount /= 2;

                var orthographicSize = camera.orthographicSize;
                camera.orthographicSize = Mathf.Lerp(orthographicSize,
                    Mathf.Clamp(orthographicSize + zoomAmount, maxZoom, minZoom), Time.fixedDeltaTime * lerpSpeed);
                zoomAmount = 0;
            }
        }

        private static Vector2 AbsoluteV2(Vector2 vector2)
        {
            return new Vector2(Mathf.Abs(vector2.x), Mathf.Abs(vector2.y));
        }

        private void OnEnable()
        {
            Forward = Vector3.ProjectOnPlane(camera.transform.forward, Vector3.up).normalized;
            Right = Vector3.ProjectOnPlane(camera.transform.right, Vector3.up).normalized;
        }
    }
}