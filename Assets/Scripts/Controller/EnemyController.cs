﻿using System;
using Character;
using Game;
using UnityEngine;
using UnityEngine.AI;

namespace Controller
{
    public class EnemyController : MonoBehaviour
    {
        public FSM fsm;

        [SerializeField] public EnemyCharacter enemy;

        [SerializeField] public NavMeshAgent navMeshAgent;

        [SerializeField] public int patrolPathIndex;
        [SerializeField] private int patrolPathPositionIndex = -1;

        [SerializeField] private PlayerCharacter target;
        private float coolDownTimer;
        private bool hasTarget;
        private float warmUpTimer;

        public enum AttackState
        {
            Warmup,
            Cooldown,
            Attack
        }

        public AttackState attackState;
        private bool isgameManagerNotNull;

        [SerializeField] public LevelManager thisLevelManager;

        private void Awake()
        {
            fsm = new FSM();

            navMeshAgent.updatePosition = false;
            navMeshAgent.updateRotation = false;


            EnemyStates.EnemyStatesRef.enemies.Add(this);
            enemy.Died += EnemyStates.EnemyStatesRef.EnemyDied;
        }

        private void Start()
        {
            isgameManagerNotNull = GameManager.gameManager != null;
            enemy.Died += delegate { fsm.MoveNext(Command.Die); };
            enemy.Started += delegate { fsm.MoveNext(Command.Awake); };

            /*
            if (thisLevelManager.patrolPoints != null)
                if (patrolPathIndex > thisLevelManager.patrolPoints.Count || patrolPathIndex < 0)
                    Debug.LogError("The specified patrol path index is not a valid patrol path", this);
             */
        }

        private void FixedUpdate()
        {
            Vector3 dir;

            switch (fsm.CurrentState)
            {
                case State.Patrol:
                    //check distance to the various player characters
                    //reaper
                    if (isgameManagerNotNull && Vector3.Distance(transform.position,
                            GameManager.gameManager.Reaper.transform.position) <
                        enemy.viewDistance)
                    {
                        target = GameManager.gameManager.Reaper;
                        fsm.MoveNext(Command.SeeReaper);
                        hasTarget = true;
                        patrolPathPositionIndex = -1;
                    }

                    //soul
                    if (isgameManagerNotNull && Vector3.Distance(transform.position,
                            GameManager.gameManager.Soul.transform.position) <
                        enemy.viewDistance)
                    {
                        target = GameManager.gameManager.Soul;
                        fsm.MoveNext(Command.SeeSoul);
                        hasTarget = true;
                        patrolPathPositionIndex = -1;
                    }

                    if (thisLevelManager.patrolPoints != null &&
                        patrolPathIndex < thisLevelManager.patrolPoints.Count && patrolPathIndex >= 0)
                    {
                        //no target found, move to closest patrol point
                        if (!hasTarget && patrolPathPositionIndex == -1)
                        {
                            patrolPathPositionIndex = thisLevelManager.patrolPoints[patrolPathIndex]
                                .GetClosestPoint(transform.position);
                        }
                        else
                        {
                            //get direction to next patrol point
                            dir = GetDirection(thisLevelManager.patrolPoints[patrolPathIndex]
                                .points[patrolPathPositionIndex]);
                            //check if we are pretty close, and change to the next patrol point
                            if (dir.magnitude < 0.5f)
                            {
                                //update the patrol point
                                patrolPathPositionIndex = (patrolPathPositionIndex + 1) %
                                                          thisLevelManager.patrolPoints[patrolPathIndex].points
                                                              .Count;
                                //get direction to new patrol point
                                dir = GetDirection(thisLevelManager.patrolPoints[patrolPathIndex]
                                    .points[patrolPathPositionIndex]);
                            }

                            //move to direction
                            enemy.Move(dir);
                        }
                    }

                    break;
                case State.TrackingSoul:
                    //if sees reaper, switch to reaper if heavy 
                    //TEMPORARY REMOVAL FOR TESTING

                    /*if (isgameManagerNotNull && enemy.enemyType == EnemyCharacter.EnemyType.Heavy &&
                        Vector3.Distance(transform.position, GameManager.gameManager.Reaper.transform.position) <
                        enemy.viewDistance)
                    {
                        fsm.MoveNext(Command.SeeReaper);
                    }
                    */
                    //if in attack range attack
                    if (Vector3.Distance(transform.position, target.transform.position) < enemy.attackRange)
                    {
                        fsm.MoveNext(Command.Attack);
                    }
                    //if soul is outside its 'view' range, lose or soul is downed and we are not sucking
                    else if (Vector3.Distance(transform.position, target.transform.position) > enemy.viewDistance ||
                             (target is Soul soul && soul.IsDown && !enemy.IsSuckingSoul))
                    {
                        fsm.MoveNext(Command.LoseSoul);
                        target = null;
                        hasTarget = false;
                    }
                    else
                    {
                        //find new direction
                        dir = GetDirection(target.transform.position);
                        //move to direction
                        enemy.Move(dir);
                    }

                    break;
                case State.TrackingReaper:
                    
                    //ADDED FOR TESTING
                    //if soul is closer than reaper, swap focus
                    if (isgameManagerNotNull &&  Vector3.Distance(transform.position, 
                        GameManager.gameManager.Soul.transform.position) < Vector3.Distance(transform.position,
                        GameManager.gameManager.Reaper.transform.position))
                    {
                        fsm.MoveNext(Command.SeeSoul);
                    }
                    //if reaper is outside its 'view' range, lose
                    if (Vector3.Distance(transform.position, target.transform.position) > enemy.viewDistance)
                    {
                        fsm.MoveNext(Command.LoseReaper);
                        target = null;
                        hasTarget = false;
                    }
                    //if in attack range attack
                    else if (Vector3.Distance(transform.position, target.transform.position) < enemy.attackRange)
                    {
                        fsm.MoveNext(Command.Attack);
                    }
                    else
                    {
                        //find new direction
                        dir = GetDirection(target.transform.position);
                        //move to direction
                        enemy.Move(dir);
                    }

                    break;
                case State.Dead:
                case State.Start:
                    enemy.Move(Vector3.zero);
                    break;
                case State.Attack:
                    //face the player
                    dir = GetDirection(target.transform.position).normalized;
                    enemy.Move(dir * 0.1f);

                    switch (attackState)
                    {
                        case AttackState.Warmup:
                            warmUpTimer += Time.fixedDeltaTime;
                            if (warmUpTimer > enemy.attackWarmup)
                            {
                                attackState = AttackState.Attack;
                                warmUpTimer = 0;
                            }

                            break;
                        case AttackState.Cooldown:
                            coolDownTimer += Time.fixedDeltaTime;
                            if (coolDownTimer > enemy.attackCooldown)
                            {
                                attackState = AttackState.Warmup;
                                coolDownTimer = 0;
                            }

                            break;
                        case AttackState.Attack:
                            enemy.Attack();
                            attackState = AttackState.Cooldown;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    //if outside attack range move to tracking
                    if (Vector3.Distance(transform.position, target.transform.position) > enemy.attackRange &&
                        coolDownTimer < enemy.attackCooldown)
                    {
                        if (target is Soul)
                        {
                            fsm.MoveNext(Command.SeeSoul);
                        }
                        else if (target is Reaper)
                        {
                            fsm.MoveNext(Command.SeeReaper);
                        }
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Vector3 GetDirection(Vector3 newTarget)
        {
            navMeshAgent.nextPosition = transform.position;

            navMeshAgent.SetDestination(newTarget);

            if (navMeshAgent.path.corners.Length >= 2)
                newTarget = navMeshAgent.path.corners[1];
            else newTarget = transform.position;


            var dir = newTarget - transform.position;
            if (dir.magnitude > 1)
                dir.Normalize();

            return dir;
        }
    }
}