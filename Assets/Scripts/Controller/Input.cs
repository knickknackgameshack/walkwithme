﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using Character;
using Game;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace Controller
{
    public class Input : MonoBehaviour
    {
        public enum ControlMode
        {
            None = -1,
            Default = 0,
            SingleController = 1,
            Keyboard = 2
        }

        [SerializeField] private ControlMode reaperControlMode;
        [SerializeField] private ControlMode soulControlMode;

        private InputDevice reaperDevice;
        private InputDevice soulDevice;

        private Action<InputAction.CallbackContext> paused => _ => Pause();


        #region Reaper Actions

        public static Action<Vector2> ReaperMove;
        public static Action ReaperInteract;
        public static Action ReaperLightAttack;
        public static Action ReaperHeavyAttack;
        public static Action<PlayerCharacter.EmoteType> ReaperEmote;
        public static Action<bool> ReaperSprint;
        public static Action ReaperParry;
        public static Action<bool> ReaperInteractHold;

        private Action<InputAction.CallbackContext> reaperInteract => _ => ReaperInteract();
        private Action<InputAction.CallbackContext> reaperLightAttack => _ => ReaperLightAttack();
        private Action<InputAction.CallbackContext> reaperHeavyAttack => _ => ReaperHeavyAttack();

        private Action<InputAction.CallbackContext> reaperEmotePoint =>
            _ => ReaperEmote(PlayerCharacter.EmoteType.Point);

        private Action<InputAction.CallbackContext> reaperEmotePose => _ => ReaperEmote(PlayerCharacter.EmoteType.Pose);
        private Action<InputAction.CallbackContext> reaperEmoteWave => _ => ReaperEmote(PlayerCharacter.EmoteType.Wave);
        private Action<InputAction.CallbackContext> reaperParry => _ => ReaperParry();

        #endregion

        #region Soul Actions

        public static Action<Vector2> SoulMove;
        public static Action SoulInteract;
        public static Action<PlayerCharacter.EmoteType> SoulEmote;
        public static Action<bool> SoulSprint;


        private Action<InputAction.CallbackContext> soulInteract => _ => SoulInteract();
        private Action<InputAction.CallbackContext> soulEmotePose => _ => SoulEmote(PlayerCharacter.EmoteType.Pose);
        private Action<InputAction.CallbackContext> soulEmoteWave => _ => SoulEmote(PlayerCharacter.EmoteType.Wave);
        private Action<InputAction.CallbackContext> soulEmotePoint => _ => SoulEmote(PlayerCharacter.EmoteType.Point);

        #endregion

        #region Generic Actions

        public static Action Pause;

        #endregion

        #region Menu Actions

        public static Action<Vector2Int> MenuMove;
        public static Action MenuConfirm;
        public static Action MenuDecline;

        #endregion

        private Controls.Reaper reaperControls;
        private Controls.Soul soulControls;
        public InputActionAsset menuControls;


        private InputUser generalUser;

        private InputUser soulPlayer;
        private InputUser reaperPlayer;

        public enum Stage
        {
            WaitingForInput,
            AssignReaper,
            AssignSoul
        }

        [SerializeField] private Stage gameStage;
        private InputAction getAnyButton;
        private MenuManager menu;

        public ControlMode ReaperControlMode
        {
            get => reaperControlMode;
            set { reaperControlMode = value; }
        }

        public ControlMode SoulControlMode
        {
            get => soulControlMode;
            set { soulControlMode = value; }
        }

        private void Awake()
        {
            reaperControls = new Controls.Reaper();
            soulControls = new Controls.Soul();
        }

        public void EnableControls()
        {
            reaperControls.Enable();
            soulControls.Enable();
        }

        public bool AssignChar(InputDevice reaper, InputDevice soul)
        {
            reaperPlayer = new InputUser();
            soulPlayer = new InputUser();

            //assign the keyboard to reaper
            reaperPlayer = InputUser.PerformPairingWithDevice(reaper, reaperPlayer,
                InputUserPairingOptions.UnpairCurrentDevicesFromUser);
            reaperPlayer.AssociateActionsWithUser(reaperControls);
            reaperControls.bindingMask = new InputBinding() {groups = "Default"};

            //assign the keyboard to soul
            soulPlayer = InputUser.PerformPairingWithDevice(soul, soulPlayer,
                InputUserPairingOptions.UnpairCurrentDevicesFromUser);
            soulPlayer.AssociateActionsWithUser(soulControls);
            soulControls.bindingMask = new InputBinding() {groups = "Default"};

            return true;
        }


        private void AnyButton(InputAction.CallbackContext ctx)
        {
            switch (gameStage)
            {
                case Stage.WaitingForInput:
                    Debug.Log(ctx.control.device + " : " + ctx.control.path);
                    //set that as the default device with an appropriate mapping
                    generalUser = new InputUser();
                    InputUser.PerformPairingWithDevice(ctx.control.device, generalUser,
                        InputUserPairingOptions.UnpairCurrentDevicesFromUser);

                    generalUser.AssociateActionsWithUser(menuControls);

                    menuControls.Enable();
                    ctx.action.Disable();
                    break;
                case Stage.AssignReaper:
                    reaperPlayer = new InputUser();
                    //set that as the default device with an appropriate mapping

                    reaperPlayer = InputUser.PerformPairingWithDevice(ctx.control.device, reaperPlayer,
                        InputUserPairingOptions.UnpairCurrentDevicesFromUser);
                    reaperPlayer.AssociateActionsWithUser(reaperControls);

                    //reaperControls.Enable();
                    if (ctx.control.device == soulDevice &&
                        (ctx.control.device.description.deviceClass != "Mouse" &&
                         ctx.control.device.description.deviceClass != "Keyboard"))
                    {
                        //do single controller
                        GameManager.gameManager.preferences.soulControlMode = ControlMode.SingleController;
                        GameManager.gameManager.preferences.reaperControlMode = ControlMode.SingleController;

                        ReaperControlMode = ControlMode.SingleController;
                        reaperControls.bindingMask = new InputBinding() {groups = "Single Controller"};

                        SoulControlMode = ControlMode.SingleController;
                        soulControls.bindingMask = new InputBinding() {groups = "Single Controller"};

                        Debug.Log(
                            "The same device was used for both soul and reaper, reassigned to single control mode");
                    }
                    else
                    {
                        switch (GameManager.gameManager.preferences.reaperControlMode)
                        {
                            case ControlMode.None:
                                break;
                            case ControlMode.Default:
                                ReaperControlMode = ControlMode.Default;
                                reaperControls.bindingMask = new InputBinding() {groups = "Default"};
                                break;
                            case ControlMode.SingleController:
                                ReaperControlMode = ControlMode.SingleController;
                                reaperControls.bindingMask = new InputBinding() {groups = "Single Controller"};
                                break;
                            case ControlMode.Keyboard:
                                if (ctx.control.device.description.deviceClass == "Mouse")
                                    reaperPlayer = InputUser.PerformPairingWithDevice(Keyboard.current, reaperPlayer);
                                ReaperControlMode = ControlMode.Keyboard;
                                reaperControls.bindingMask = new InputBinding() {groups = "Keyboard"};
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }

                    if (reaperControlMode == ControlMode.Keyboard)
                        reaperDevice = Keyboard.current;
                    else
                        reaperDevice = ctx.control.device;

                    Debug.Log(ctx.control.device + " : " + ctx.control.path + " was assigned to Reaper");
                    getAnyButton.Disable();
                    menuControls.Enable();

                    break;
                case Stage.AssignSoul:

                    soulPlayer = InputUser.PerformPairingWithDevice(ctx.control.device, soulPlayer,
                        InputUserPairingOptions.UnpairCurrentDevicesFromUser);
                    soulPlayer.AssociateActionsWithUser(soulControls);

                    //soulControls.Enable();

                    if (ctx.control.device == reaperDevice &&
                        (ctx.control.device.description.deviceClass != "Mouse" &&
                         ctx.control.device.description.deviceClass != "Keyboard"))
                    {
                        //do single controller
                        GameManager.gameManager.preferences.soulControlMode = ControlMode.SingleController;
                        GameManager.gameManager.preferences.reaperControlMode = ControlMode.SingleController;

                        ReaperControlMode = ControlMode.SingleController;
                        reaperControls.bindingMask = new InputBinding() {groups = "Single Controller"};

                        SoulControlMode = ControlMode.SingleController;
                        soulControls.bindingMask = new InputBinding() {groups = "Single Controller"};

                        Debug.Log(
                            "The same device was used for both soul and reaper, reassigned to single control mode");
                    }
                    else
                    {
                        switch (GameManager.gameManager.preferences.soulControlMode)
                        {
                            case ControlMode.None:
                                break;
                            case ControlMode.Default:
                                SoulControlMode = ControlMode.Default;
                                soulControls.bindingMask = new InputBinding() {groups = "Default"};
                                break;
                            case ControlMode.SingleController:
                                SoulControlMode = ControlMode.SingleController;
                                soulControls.bindingMask = new InputBinding() {groups = "Single Controller"};
                                break;
                            case ControlMode.Keyboard:
                                if (ctx.control.device.description.deviceClass == "Mouse")
                                    soulPlayer = InputUser.PerformPairingWithDevice(Keyboard.current, soulPlayer,
                                        InputUserPairingOptions.None);
                                SoulControlMode = ControlMode.Keyboard;
                                soulControls.bindingMask = new InputBinding() {groups = "Keyboard"};
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }

                    if (soulControlMode == ControlMode.Keyboard)
                        soulDevice = Keyboard.current;
                    else
                        soulDevice = ctx.control.device;

                    Debug.Log(ctx.control.device + " : " + ctx.control.path + " was assigned to Soul");
                    getAnyButton.Disable();
                    menuControls.Enable();

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public List<ControlMode> GetControlModes()
        {
            var controlModes = new List<ControlMode>();

            if (InputSystem.GetDevice<Keyboard>() != null) controlModes.Add(ControlMode.Keyboard);

            if (Gamepad.all.Count >= 1) controlModes.Add(ControlMode.SingleController);
            if (Gamepad.all.Count >= 2) controlModes.Add(ControlMode.Default);


            return controlModes;
        }

        /// <summary>
        /// Method of setting up the controls for the system
        /// </summary>
        /// <param name="controlType">The type of controls requested</param>
        /// <returns>Whether the binding was a success, returning false means there was either no compatible devices found, or the controlType is unsupported </returns>
        public bool SetupControls(ControlMode controlType)
        {
            //we want a keyboard
            if (controlType == ControlMode.Keyboard)
            {
                //but there isn't one ^o^
                if (InputSystem.GetDevice<Keyboard>() == null) return false;
                //create new input users for the assignment
                reaperPlayer = new InputUser();
                soulPlayer = new InputUser();

                //assign the keyboard to reaper
                reaperPlayer = InputUser.PerformPairingWithDevice(Keyboard.current, reaperPlayer,
                    InputUserPairingOptions.UnpairCurrentDevicesFromUser);
                reaperPlayer.AssociateActionsWithUser(reaperControls);
                reaperControls.bindingMask = new InputBinding() {groups = "Keyboard"};


                //assign the keyboard to soul
                soulPlayer = InputUser.PerformPairingWithDevice(Keyboard.current, soulPlayer);
                soulPlayer.AssociateActionsWithUser(soulControls);
                soulControls.bindingMask = new InputBinding() {groups = "Keyboard"};

                return true;
            }

            //there are nno gamepads connected and the requested control mode requires a gamepad
            if (controlType == ControlMode.Default || controlType == ControlMode.SingleController &&
                InputSystem.GetDevice<Gamepad>() == null) return false;

            //we want a single controller for both players
            if (controlType == ControlMode.Default)
            {
                var controllers = Gamepad.all.ToList();
                //there isn't enough controllers for everyone
                if (controllers.Count() < 2) return false;

                var reaperGamepad = controllers[0];
                var soulGamepad = controllers[1];

                return AssignChar(reaperGamepad, soulGamepad);
            }

            //if we are here then there is at least one controller
            if (controlType == ControlMode.SingleController)
            {
                var controller = Gamepad.current;

                reaperPlayer = new InputUser();
                soulPlayer = new InputUser();

                //assign the keyboard to reaper
                reaperPlayer = InputUser.PerformPairingWithDevice(controller, reaperPlayer,
                    InputUserPairingOptions.UnpairCurrentDevicesFromUser);
                reaperPlayer.AssociateActionsWithUser(reaperControls);
                reaperControls.bindingMask = new InputBinding() {groups = "Single Controller"};

                //assign the keyboard to soul
                soulPlayer = InputUser.PerformPairingWithDevice(controller, soulPlayer,
                    InputUserPairingOptions.UnpairCurrentDevicesFromUser);
                soulPlayer.AssociateActionsWithUser(soulControls);
                soulControls.bindingMask = new InputBinding() {groups = "Single Controller"};

                return true;
            }

            return false;
        }

        public List<ControlMode> CheckCompatibility()
        {
            var list = new List<ControlMode>();

            var devices = InputSystem.devices.ToArray();

            if (devices.Any(d => d is Keyboard)) list.Add(ControlMode.Keyboard);
            if (devices.Any(d => d is Gamepad))
            {
                list.Add(ControlMode.SingleController);
                if (devices.Count(d => d is Gamepad) >= 2)
                    list.Add(ControlMode.Default);
            }

            return list;
        }


        public void SetReaperController()
        {
            gameStage = Stage.AssignReaper;
            menuControls.Disable();
            getAnyButton = new InputAction(binding: "/*/<button>", interactions: "tap");
            getAnyButton.performed += AnyButton;
            getAnyButton.Enable();
        }

        public void SetSoulController()
        {
            gameStage = Stage.AssignSoul;
            menuControls.Disable();
            getAnyButton = new InputAction(binding: "/*/<button>", interactions: "tap");
            getAnyButton.performed += AnyButton;
            getAnyButton.Enable();
        }


        private void FixedUpdate()
        {
            if (reaperControls.Gameplay.enabled)
            {
                ReaperMove?.Invoke(reaperControls.Gameplay.Move.ReadValue<Vector2>());
                ReaperSprint?.Invoke(reaperControls.Gameplay.Sprint.ReadValue<float>() > 0);
                ReaperInteractHold?.Invoke(reaperControls.Gameplay.Interact.ReadValue<float>() > 0.1f);
            }

            if (soulControls.Gameplay.enabled)
            {
                SoulMove?.Invoke(soulControls.Gameplay.Move.ReadValue<Vector2>());
                SoulSprint?.Invoke(soulControls.Gameplay.Sprint.ReadValue<float>() > 0);
            }
        }

        private void OnEnable()
        {
            reaperControls.Gameplay.Interact.performed += reaperInteract;
            reaperControls.Gameplay.Parry.performed += reaperParry;
            reaperControls.Gameplay.HeavyAttack.performed += reaperHeavyAttack;
            reaperControls.Gameplay.LightAttack.performed += reaperLightAttack;
            reaperControls.Gameplay.Emote_Point.performed += reaperEmotePoint;
            reaperControls.Gameplay.Emote_Pose.performed += reaperEmotePose;
            reaperControls.Gameplay.Emote_Wave.performed += reaperEmoteWave;
            soulControls.Gameplay.Interact.performed += soulInteract;
            soulControls.Gameplay.Emote_Point.performed += soulEmotePoint;
            soulControls.Gameplay.Emote_Pose.performed += soulEmotePose;
            soulControls.Gameplay.Emote_Wave.performed += soulEmoteWave;

            soulControls.Gameplay.Pause.performed += paused;
            reaperControls.Gameplay.Pause.performed += paused;
        }

        private void OnDisable()
        {
            soulControls.Gameplay.Interact.performed -= soulInteract;
            soulControls.Gameplay.Emote_Point.performed -= soulEmotePoint;
            soulControls.Gameplay.Emote_Pose.performed -= soulEmotePose;
            soulControls.Gameplay.Emote_Wave.performed -= soulEmoteWave;
            reaperControls.Gameplay.Interact.performed -= reaperInteract;
            reaperControls.Gameplay.Parry.performed -= reaperParry;
            reaperControls.Gameplay.HeavyAttack.performed -= reaperHeavyAttack;
            reaperControls.Gameplay.LightAttack.performed -= reaperLightAttack;
            reaperControls.Gameplay.Emote_Point.performed -= reaperEmotePoint;
            reaperControls.Gameplay.Emote_Pose.performed -= reaperEmotePose;
            reaperControls.Gameplay.Emote_Wave.performed -= reaperEmoteWave;

            soulControls.Gameplay.Pause.performed -= paused;
            reaperControls.Gameplay.Pause.performed -= paused;
        }
    }
}