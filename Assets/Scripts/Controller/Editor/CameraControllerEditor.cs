﻿using UnityEditor;
using UnityEngine;

namespace Controller.Editor
{
    [CustomEditor(typeof(CameraController))]
    public class CameraControllerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            //we still want the inspector of the default object

        }
    }
}