﻿using System.Linq;
using Character;
using Game;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace Controller.Editor
{
    [CustomEditor(typeof(EnemyController))]
    public class EnemyControllerEditor : UnityEditor.Editor
    {
        private int levelManagerIndex;

        public override void OnInspectorGUI()
        {
            GUIStyle redFont = new GUIStyle(GUI.skin.label)
                {normal = {textColor = Color.red * 0.6f}, fontStyle = FontStyle.Bold};
            GUIStyle greenFont = new GUIStyle(GUI.skin.label)
                {normal = {textColor = Color.green * 0.6f}, fontStyle = FontStyle.Bold};


            var enemy = target as EnemyController;


            EditorGUILayout.LabelField("Current FSM State");

            GUI.enabled = false;
            EditorGUILayout.EnumPopup(enemy.fsm.CurrentState);
            if (enemy.fsm.CurrentState == State.Attack)
            {
                EditorGUILayout.EnumPopup(enemy.attackState);
            }

            GUI.enabled = true;

            EditorGUILayout.Space();
            //enemy controller
            enemy.enemy = enemy.GetComponent<EnemyCharacter>();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Enemy Character:");
            if (enemy.enemy == null)
                EditorGUILayout.LabelField("False", redFont);
            else EditorGUILayout.LabelField("True", greenFont);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            //enemy navmeshagent
            //enemy.navMeshAgent = enemy.GetComponent<NavMeshAgent>();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("NavMeshAgent:");
            if (enemy.navMeshAgent == null)
                EditorGUILayout.LabelField("False", redFont);
            else EditorGUILayout.LabelField("True", greenFont);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            //patrol path choose
            var lm = FindObjectOfType<LevelManager>();
            if (enemy.thisLevelManager != lm)
            {
                enemy.thisLevelManager = lm;
                EditorUtility.SetDirty(target);
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Level Manager:");
            if (enemy.thisLevelManager != null) EditorGUILayout.LabelField("Found", greenFont);
            else EditorGUILayout.LabelField("Not Found", redFont);
            EditorGUILayout.EndHorizontal();

            if (enemy.thisLevelManager != null)
            {
                var tempNames = enemy.thisLevelManager.patrolPoints.Select(n => n.name).ToArray();
                levelManagerIndex = tempNames.Length == 0 ? 0 : levelManagerIndex;

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Patrol Path");

                if (enemy.patrolPathIndex < tempNames.Length) levelManagerIndex = enemy.patrolPathIndex;

                levelManagerIndex = EditorGUILayout.Popup(levelManagerIndex, tempNames.Length > 0
                    ? tempNames
                    : new[] {""});

                EditorGUILayout.EndHorizontal();
                if (levelManagerIndex != enemy.patrolPathIndex)
                {
                    enemy.patrolPathIndex = levelManagerIndex;
                    EditorUtility.SetDirty(target);
                }
            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Level Manager:");
                EditorGUILayout.LabelField("Not Found", redFont);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}