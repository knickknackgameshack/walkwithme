﻿using System;
using UnityEngine;

namespace Controller
{
    public class CameraFocus : MonoBehaviour
    {
        [SerializeField] private float targetZoom;

        private void OnEnable()
        {
            CameraController.OverrideFocusPoint = transform;
            CameraController.UseOverride = true;
            CameraController.TargetZoom = targetZoom;
        }

        private void OnDisable()
        {
            CameraController.UseOverride = false;
            CameraController.OverrideFocusPoint = null;
        }
    }
}