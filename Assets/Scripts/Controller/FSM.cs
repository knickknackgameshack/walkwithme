﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Controller
{
    public enum State
    {
        Start,
        Patrol,
        TrackingSoul,
        TrackingReaper,
        Attack,
        Dead
    }

    public enum Command
    {
        Awake,
        Die,
        SeeSoul,
        SeeReaper,
        LoseSoul,
        LoseReaper,
        Attack
    }

    [Serializable]
    public class FSM
    {
        private Dictionary<StateTransition, State> transitions;

        public FSM()
        {
            Process();
        }

        public State CurrentState { get; private set; }

        public void Process()
        {
            CurrentState = State.Start;
            transitions = new Dictionary<StateTransition, State>
            {
                //patrol state
                {new StateTransition(State.Patrol, Command.SeeReaper), State.TrackingReaper},
                {new StateTransition(State.Patrol, Command.SeeSoul), State.TrackingSoul},
                {new StateTransition(State.Patrol, Command.Die), State.Dead},
                //tracking soul state
                {new StateTransition(State.TrackingSoul, Command.SeeReaper), State.TrackingReaper},
                {new StateTransition(State.TrackingSoul, Command.LoseSoul), State.Patrol},
                {new StateTransition(State.TrackingSoul, Command.Attack), State.Attack},
                {new StateTransition(State.TrackingSoul, Command.Die), State.Dead},
                //tracking reaper state
                {new StateTransition(State.TrackingReaper, Command.SeeSoul), State.TrackingSoul},
                {new StateTransition(State.TrackingReaper, Command.LoseReaper), State.Patrol},
                {new StateTransition(State.TrackingReaper, Command.Attack), State.Attack},
                {new StateTransition(State.TrackingReaper, Command.Die), State.Dead},
                //attack -> lose && attack -> track
                {new StateTransition(State.Attack, Command.SeeReaper), State.TrackingReaper},
                {new StateTransition(State.Attack, Command.SeeSoul), State.TrackingSoul},
                {new StateTransition(State.Attack, Command.LoseReaper), State.Patrol},
                {new StateTransition(State.Attack, Command.LoseSoul), State.Patrol},
                {new StateTransition(State.Attack, Command.Die), State.Dead},
                //Start 
                {new StateTransition(State.Start, Command.Awake), State.Patrol},
                {new StateTransition(State.Start, Command.Die), State.Dead},
                //Dead
                {new StateTransition(State.Dead, Command.Die), State.Dead},
            };
        }

        public State GetNext(Command command)
        {
            var transition = new StateTransition(CurrentState, command);
            State nextState;
            if (!transitions.TryGetValue(transition, out nextState))
                throw new Exception("Invalid transitions: " + CurrentState + " -> " + command);
            return nextState;
        }

        public State MoveNext(Command command)
        {
            CurrentState = GetNext(command);
            return CurrentState;
        }

        private class StateTransition
        {
            private readonly Command Command;
            private readonly State CurrentState;

            public StateTransition(State currentState, Command command)
            {
                CurrentState = currentState;
                Command = command;
            }

            public override int GetHashCode()
            {
                return 17 + 31 * CurrentState.GetHashCode() + 31 * Command.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                var other = obj as StateTransition;
                return other != null && CurrentState == other.CurrentState && Command == other.Command;
            }
        }
    }
}