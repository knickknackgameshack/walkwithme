﻿using Character;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Controller
{
    public class SoulController : PlayerController
    {
        [SerializeField] private Soul soul;

        public Soul Soul
        {
            get => soul;
            set => soul = value;
        }

        private void OnEnable()
        {
            Input.SoulMove += SoulMove;
            Input.SoulInteract += SoulInteract;
            Input.SoulEmote += SoulEmote;
            Input.SoulSprint += SoulSprint;
        }

        private void SoulSprint(bool sprint)
        {
            soul.Sprint(sprint);
        }


        private void SoulEmote(PlayerCharacter.EmoteType emote)
        {
            soul.Emote(emote);
        }

        private void SoulInteract()
        {
            soul.Interact();
        }

        private void SoulMove(Vector2 dir)
        {
            soul.Move(dir.x * CameraController.Right + dir.y * CameraController.Forward);
        }

        private void OnDisable()
        {
            Input.SoulMove -= SoulMove;
            Input.SoulInteract -= SoulInteract;
            Input.SoulEmote -= SoulEmote;
            Input.SoulSprint -= SoulSprint;
        }
    }
}