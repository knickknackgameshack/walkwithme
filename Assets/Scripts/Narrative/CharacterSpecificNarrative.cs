﻿using System;
using System.Collections.Generic;
using Character;
using UnityEngine;

namespace Narrative
{
    public class CharacterSpecificNarrative : MonoBehaviour
    {
        [SerializeField] private Player player;

        private List<Character.Character> characters = new List<Character.Character>();


        private void OnTriggerEnter(Collider other)
        {
            switch (player)
            {
                case Player.None:
                    break;
                case Player.Soul:
                    if (other.GetComponent<Character.Character>() is Soul soul && !characters.Contains(soul))
                    {
                        characters.Add(soul);
                        soul.Interacted += Interacted;
                    }

                    break;
                case Player.Reaper:
                    if (other.GetComponent<Character.Character>() is Reaper reaper && !characters.Contains(reaper))
                    {
                        characters.Add(reaper);
                        reaper.Interacted += Interacted;
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected virtual void Interacted(PlayerCharacter obj)
        {
            //todo do the popup
        }

        private void OnTriggerExit(Collider other)
        {
            switch (player)
            {
                case Player.None:
                    break;
                case Player.Soul:
                    if (other.GetComponent<Character.Character>() is Soul soul && characters.Contains(soul))
                    {
                        soul.Interacted -= Interacted;
                        characters.Remove(soul);
                    }

                    break;
                case Player.Reaper:
                    if (other.GetComponent<Character.Character>() is Reaper reaper && characters.Contains(reaper))
                    {
                        reaper.Interacted -= Interacted;
                        characters.Remove(reaper);
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}