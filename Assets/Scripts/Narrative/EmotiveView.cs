﻿using Controller;
using UnityEngine;
using UnityEngine.UI;
using Puzzle;
using System.Collections.Generic;

namespace Narrative
{
    public class EmotiveView : MonoBehaviour
    {
        [SerializeField] private Color correctColor;
        [SerializeField] private Color incorrectColor;

        [SerializeField] private Image dialGlyph;
        [SerializeField] private Image pressureGlyph;
        [SerializeField] private Image leverGlyph;
        [SerializeField] private Image altarGlyph;
        [SerializeField] private Image defaultGlyph;

        [SerializeField] private HorizontalLayoutGroup correctPuzzles;
        [SerializeField] private HorizontalLayoutGroup incorrectPuzzles;

        private Vector2 glyphSize = new Vector2(0.22f, 0.22f);

        private void FixedUpdate()
        {
            //align to camera plane
            transform.rotation = Quaternion.LookRotation(CameraController.Forward, Vector3.up);
        }

        public void ShowPuzzle(List<Switch> switchList)
        {
            ClearPuzzle();
            gameObject.SetActive(true);


            foreach (var @switch in switchList)
            {
                Image image = null;

                switch (@switch)
                {
                    case DialSwitch dial:
                        image = Instantiate(dialGlyph,
                            @switch.state.State ? correctPuzzles.transform : incorrectPuzzles.transform, false);
                        break;
                    case LeverSwitch lever:
                        image = Instantiate(leverGlyph,
                            @switch.state.State ? correctPuzzles.transform : incorrectPuzzles.transform, false);

                        break;
                    case PressurePlateSwitch pressurePlate:
                        image = Instantiate(pressureGlyph,
                            @switch.state.State ? correctPuzzles.transform : incorrectPuzzles.transform, false);

                        break;
                    case BloodAltar bloodAltar:
                        image = Instantiate(altarGlyph,
                            @switch.state.State ? correctPuzzles.transform : incorrectPuzzles.transform, false);

                        break;
                    default:
                        image = Instantiate(defaultGlyph,
                            @switch.state.State ? correctPuzzles.transform : incorrectPuzzles.transform, false);
                        break;
                }

                image.color = @switch.state.State ? correctColor : incorrectColor;
                image.rectTransform.sizeDelta = glyphSize;
            }
        }

        private void ClearPuzzle()
        {
            int child = correctPuzzles.transform.childCount;
            for (int i = child - 1; i >= 0; i--)
            {
                Destroy(correctPuzzles.transform.GetChild(i).gameObject);
            }

            child = incorrectPuzzles.transform.childCount;
            for (int i = child - 1; i >= 0; i--)
            {
                Destroy(incorrectPuzzles.transform.GetChild(i).gameObject);
            }
        }

        public void HidePuzzle()
        {
            gameObject.SetActive(false);
        }
    }
}