﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Narrative.Editor
{
    [CustomEditor(typeof(TextDictionary))]
    public class TextDictionaryCustomEditor : UnityEditor.Editor
    {
        private string keyName = "";

        private Dictionary<string, bool> foldoutOpen;

        public override void OnInspectorGUI()
        {
            var narrative = target as TextDictionary;

            narrative.Language = (Language) EditorGUILayout.EnumPopup(narrative.Language);

            //if the dictionary isnt initialized yet
            if (narrative.Text == null)
            {
                narrative.Text = new Dictionary<string, Dictionary<Language, string>>();
            }

            GUILayout.BeginHorizontal();

            keyName = EditorGUILayout.TextField(keyName);
            if (GUILayout.Button("Create Key", GUILayout.ExpandWidth(true)) && !narrative.Text.ContainsKey(keyName))
            {
                narrative.Text.Add(keyName, new Dictionary<Language, string>());
                foreach (var language in (Language[]) Enum.GetValues(typeof(Language)))
                {
                    narrative.Text[keyName].Add(language, "");
                }
            }

            GUILayout.EndHorizontal();

            var keys = narrative.Text.Keys.ToList();

            if (foldoutOpen == null) foldoutOpen = new Dictionary<string, bool>();

            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                if (!foldoutOpen.ContainsKey(key)) foldoutOpen.Add(key, false);

                // ReSharper disable once AssignmentInConditionalExpression
                if (foldoutOpen[key] = EditorGUILayout.Foldout(foldoutOpen[key], key))
                {
                    //if that language isnt in the dictionary yet,,
                    if (!narrative.Text[key].ContainsKey(narrative.Language))
                    {
                        narrative.Text[key].Add(narrative.Language, "");
                        EditorUtility.SetDirty(narrative);
                    }

                    var temp = narrative.Text[key][narrative.Language];
                    narrative.Text[key][narrative.Language] =
                        EditorGUILayout.TextArea(narrative.Text[key][narrative.Language], GUILayout.MinHeight(60));

                    if (narrative.Text[key][narrative.Language] != temp)
                        EditorUtility.SetDirty(narrative);

                    if (GUILayout.Button("Delete"))
                    {
                        narrative.Text.Remove(key);
                        EditorUtility.SetDirty(narrative);
                        break;
                    }
                }
            }
        }
    }

    public class CreateTextDictionaryMenu
    {
        [MenuItem("Assets/Knick Knack Game Shack/Create/Text Dictionary"),
         MenuItem("Knick Knack Game Shack/Create/Text Dictionary")]
        private static void CreateTextDictionary(MenuCommand menuCommand)
        {
            //creates a new text dictionary in the appropriate location
            TextDictionary textDict = ScriptableObject.CreateInstance<TextDictionary>();
            AssetDatabase.CreateAsset(textDict, "Assets/Resources/Narrative/New Text Dictionary.asset");
            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = textDict;
        }
    }
}