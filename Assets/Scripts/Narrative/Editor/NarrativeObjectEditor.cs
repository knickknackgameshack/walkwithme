﻿using System;
using System.Collections.Generic;
using System.Linq;
using Controls;
using Game;
using UI;
using UnityEditor;
using UnityEngine;

namespace Narrative.Editor
{
    [CustomEditor(typeof(NarrativeObject))]
    public class NarrativeObjectEditor : UnityEditor.Editor
    {
        private string stage;
        private int soulChoice;
        private int soulNameChoice;
        private int reaperChoice;
        private int reaperNameChoice;
        private List<TextDictionary> assets;

        private bool soul;
        private bool reaper;


        private void OnEnable()
        {
            assets = GameManager.GetTextDictionaries();
        }

        public override void OnInspectorGUI()
        {
            var narrativeObject = target as NarrativeObject;

            soul = EditorGUILayout.Foldout(soul, "Soul Line");
            if (soul)
            {
                var tempNames = assets.Select(n => n.name).ToArray();

                soulChoice = Array.FindIndex(tempNames, n => n == narrativeObject.SoulCharacterLine.TextDictionaryName);
                soulChoice = tempNames.Length == 0 ? 0 : soulChoice;

                soulChoice = EditorGUILayout.Popup(soulChoice, tempNames.Length > 0
                    ? tempNames
                    : new[] {""});

                if (assets[soulChoice].Text == null)
                    assets[soulChoice].Text = new Dictionary<string, Dictionary<Language, string>>();


                var tempKeys = assets[soulChoice].Text.Keys.ToArray();
                soulNameChoice = Array.FindIndex(tempKeys,
                    n => n == narrativeObject.SoulCharacterLine.TextDictionaryKey);
                if (soulNameChoice == -1) soulNameChoice = 0;
                var tempChoice = soulNameChoice < tempKeys.Length ? soulNameChoice : 0;

                tempChoice = EditorGUILayout.Popup(tempChoice, tempKeys.Length > 0 ? tempKeys : new[] {""});


                if (tempChoice != soulNameChoice)
                {
                    soulNameChoice = tempChoice;

                    narrativeObject.SoulCharacterLine.TextDictionaryKey = tempKeys[soulNameChoice];
                    EditorUtility.SetDirty(target);
                }

                if (tempKeys.Length != 0)
                {
                    var temp = assets[soulChoice].Text[tempKeys[soulNameChoice]][assets[soulChoice].Language];

                    temp = EditorGUILayout.TextArea(temp, GUILayout.MinHeight(60), GUILayout.ExpandHeight(true));

                    if (temp != assets[soulChoice].Text[tempKeys[soulNameChoice]][assets[soulChoice].Language])
                    {
                        assets[soulChoice].Text[tempKeys[soulNameChoice]][assets[soulChoice].Language] = temp;
                        EditorUtility.SetDirty(assets[soulChoice]);
                    }

                    if (narrativeObject.SoulCharacterLine.TextDictionaryName != assets[soulChoice].name)
                    {
                        narrativeObject.SoulCharacterLine.TextDictionaryName = assets[soulChoice].name;
                        EditorUtility.SetDirty(narrativeObject);
                    }

                    if (narrativeObject.SoulCharacterLine.TextDictionaryKey != tempKeys[soulNameChoice])
                    {
                        narrativeObject.SoulCharacterLine.TextDictionaryKey = tempKeys[soulNameChoice];
                        EditorUtility.SetDirty(narrativeObject);
                    }
                }
            }

            EditorGUILayout.Space();

            reaper = EditorGUILayout.Foldout(reaper, "Reaper Line");
            if (reaper)
            {
                var tempNames = assets.Select(n => n.name).ToArray();

                reaperChoice = Array.FindIndex(tempNames,
                    n => n == narrativeObject.ReaperCharacterLine.TextDictionaryName);
                reaperChoice = tempNames.Length == 0 ? 0 : reaperChoice;

                reaperChoice = EditorGUILayout.Popup(reaperChoice, tempNames.Length > 0
                    ? tempNames
                    : new[] {""});

                if (assets[reaperChoice].Text == null)
                    assets[reaperChoice].Text = new Dictionary<string, Dictionary<Language, string>>();

                var tempKeys = assets[reaperChoice].Text.Keys.ToArray();
                reaperNameChoice = Array.FindIndex(tempKeys,
                    n => n == narrativeObject.ReaperCharacterLine.TextDictionaryKey);
                if (reaperNameChoice == -1) reaperNameChoice = 0;
                var tempChoice = reaperNameChoice < tempKeys.Length ? reaperNameChoice : 0;

                tempChoice = EditorGUILayout.Popup(tempChoice, tempKeys.Length > 0 ? tempKeys : new[] {""});


                if (tempChoice != reaperNameChoice)
                {
                    reaperNameChoice = tempChoice;

                    narrativeObject.ReaperCharacterLine.TextDictionaryKey = tempKeys[reaperNameChoice];
                    EditorUtility.SetDirty(target);
                }

                if (tempKeys.Length != 0)
                {
                    var temp = assets[reaperChoice].Text[tempKeys[reaperNameChoice]][assets[reaperChoice].Language];

                    temp = EditorGUILayout.TextArea(temp, GUILayout.MinHeight(60), GUILayout.ExpandHeight(true));

                    if (temp != assets[reaperChoice].Text[tempKeys[reaperNameChoice]][assets[reaperChoice].Language])
                    {
                        assets[reaperChoice].Text[tempKeys[reaperNameChoice]][assets[reaperChoice].Language] = temp;
                        EditorUtility.SetDirty(assets[reaperChoice]);
                    }

                    if (narrativeObject.ReaperCharacterLine.TextDictionaryName != assets[reaperChoice].name)
                    {
                        narrativeObject.ReaperCharacterLine.TextDictionaryName = assets[reaperChoice].name;
                        EditorUtility.SetDirty(narrativeObject);
                    }

                    if (narrativeObject.ReaperCharacterLine.TextDictionaryKey != tempKeys[reaperNameChoice])
                    {
                        narrativeObject.ReaperCharacterLine.TextDictionaryKey = tempKeys[reaperNameChoice];
                        EditorUtility.SetDirty(narrativeObject);
                    }
                }
            }

            EditorGUILayout.Space();
            narrativeObject.canvas = (WorldSpaceCanvasObject) EditorGUILayout.ObjectField("World Space Prefab",
                narrativeObject.canvas, typeof(WorldSpaceCanvasObject), false);

            //base.OnInspectorGUI();
        }
    }
}