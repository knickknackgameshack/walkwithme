﻿using System;
using Game;
using UnityEngine;
using Character;
using Controller;
using System.Collections.Generic;
using UI;

namespace Narrative
{
    [Serializable]
    public struct CharacterReference
    {
        [SerializeField] private string textDictionaryName;
        [SerializeField] private string textDictionaryKey;

        public string TextDictionaryName
        {
            get => textDictionaryName;
            set => textDictionaryName = value;
        }

        public string TextDictionaryKey
        {
            get => textDictionaryKey;
            set => textDictionaryKey = value;
        }
    }

    public enum Player
    {
        None,
        Soul,
        Reaper,
    }

    public class NarrativeObject : MonoBehaviour
    {
        public CharacterReference SoulCharacterLine;
        public CharacterReference ReaperCharacterLine;

        private string Text(Player player) => GameManager.GetDialogue(
            player == Player.Reaper ? (ReaperCharacterLine.TextDictionaryName) : SoulCharacterLine.TextDictionaryName,
            player == Player.Reaper ? (ReaperCharacterLine.TextDictionaryKey) : SoulCharacterLine.TextDictionaryKey
        );

        public WorldSpaceCanvasObject canvas;
        private List<PlayerCharacter> players = new List<PlayerCharacter>();

        private Dictionary<PlayerCharacter, WorldSpaceCanvasObject> WorldSpaceCanvasDictionary =
            new Dictionary<PlayerCharacter, WorldSpaceCanvasObject>();

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                var tempPlayer = other.gameObject.GetComponent<PlayerCharacter>();
                players.Add(tempPlayer);
                tempPlayer.Interacted += PlayerOnInteracted;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                var tempPlayer = other.gameObject.GetComponent<PlayerCharacter>();
                if (players.Contains(tempPlayer))
                {
                    tempPlayer.Interacted -= PlayerOnInteracted;
                    players.Remove(tempPlayer);

                    if (WorldSpaceCanvasDictionary.ContainsKey(tempPlayer))
                    {
                        Destroy(WorldSpaceCanvasDictionary[tempPlayer].gameObject);
                        WorldSpaceCanvasDictionary.Remove(tempPlayer);
                    }
                }
            }
        }

        private void PlayerOnInteracted(PlayerCharacter player)
        {
            if (!WorldSpaceCanvasDictionary.ContainsKey(player))
            {
                WorldSpaceCanvasDictionary.Add(player,
                    Instantiate(canvas, transform.position + Vector3.up * 0.5f,
                        Quaternion.LookRotation(CameraController.Forward)));
                WorldSpaceCanvasDictionary[player].textBox.text = Text(GetPlayerType(player));
                if (GameManager.gameManager.ReaperFont != null && GameManager.gameManager.SoulFont != null)
                {
                    WorldSpaceCanvasDictionary[player].textBox.font = player is Reaper
                        ? GameManager.gameManager.ReaperFont
                        : GameManager.gameManager.SoulFont;
                }

                if (WorldSpaceCanvasDictionary.Values.Count >= 2)
                {
                    foreach (var key in WorldSpaceCanvasDictionary.Keys)
                    {
                        if (key != player)
                        {
                            Destroy(WorldSpaceCanvasDictionary[key].gameObject);
                            WorldSpaceCanvasDictionary.Remove(key);
                        }
                    }
                }

                //player.interacting = true;
            }
            else
            {
                Destroy(WorldSpaceCanvasDictionary[player].gameObject);
                WorldSpaceCanvasDictionary.Remove(player);
                //player.interacting = false;
            }

            Debug.Log(player.Interacting);
        }

        public static Player GetPlayerType(PlayerCharacter player)
        {
            if (player is Soul) return Player.Soul;
            if (player is Reaper) return Player.Reaper;
            else return Player.None;
        }
    }
}