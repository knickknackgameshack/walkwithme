﻿using System.Collections.Generic;
using System.Linq;
using Character;
using Puzzle;
using UnityEngine;

namespace Narrative
{
    public class GateInteractive : CharacterSpecificNarrative
    {
        [SerializeField] private List<Switch> switches;


        protected override void Interacted(PlayerCharacter obj)
        {
            if (obj is Reaper reaper)
            {
                reaper.Gate = this;
                ShowStatus(reaper);
            }
        }

        public void ShowStatus(Reaper reaper)
        {
            reaper.emotiveView.ShowPuzzle(switches);
        }

    }
}