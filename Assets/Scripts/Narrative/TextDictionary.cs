﻿using System.Collections.Generic;
using UnityEngine;
using OdinSerializer;

namespace Narrative
{
    public enum Language
    {
        English
    }

    [CreateAssetMenu(fileName = "TextDictionary", menuName = "Create Text Dictionary", order = 0)]
    public class TextDictionary : SerializedScriptableObject
    {
        public Language Language;

        [SerializeField] public Dictionary<string, Dictionary<Language, string>> Text;
    }
}