﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using UnityEngine;

namespace Character
{
    public enum EmoteType
    {
        Wave,
        Pose,
        Point
    }

    [RequireComponent(typeof(Rigidbody))]
    public abstract class Character : MonoBehaviour
    {
        #region Health

        [SerializeField] private int health;
        [SerializeField, Range(1, 10)] private int maxHealth;

        [SerializeField, Tooltip("How long the character is invincible for after taking damage"), Range(0, 5)]
        protected float invincibleTime = 0.5f;

        public bool Invincible => invincible;

        protected bool invincible = false;

        public int MaxHealth => maxHealth;

        public bool Attacking;

        public int Health
        {
            get => health;
            set
            {
                if (invincible && value < health) return; //when invincible the character cannot take damage
                if (health > value && health != 0)
                {
                    //health is going down
                    //play hurt anim
                    if (isAnimatorNotNull) anim.SetTrigger(Hurt);
                    timeSinceDamage = 0;
                }
                else if (health > value)
                {
                    timeSinceDamage = 0;
                }

                if (value <= 0)
                {
                    health = 0;
                    Died?.Invoke(this);
                    HasDied();
                }
                else if (value >= maxHealth) health = maxHealth;
                else health = value;

                StartCoroutine(InvincibleTimer()); //begin the invincible timer
                HealthChanged?.Invoke(health);
            }
        }

        //Action for when the health has changed, useful for UI changes
        public Action<int> HealthChanged;

        //Action for when the character has died, used for knowing when the character no longer exists
        public Action<Character> Died;

        protected Action TakeDamage;

        protected IEnumerator InvincibleTimer()
        {
            invincible = true;
            yield return new WaitForSeconds(invincibleTime);
            invincible = false;
        }

        protected virtual void Update()
        {
            timeSinceDamage += Time.deltaTime;

            damageRenderers.ForEach(r =>
            {
                r.GetPropertyBlock(prop);
                prop.SetFloat("_damageTime", timeSinceDamage);
                r.SetPropertyBlock(prop);
            });
        }

        #endregion

        #region Movement

        [SerializeField, Range(0, 10)] protected float maxSpeed = 3;
        [SerializeField, Range(0, 5)] private float movementAcceleration = 1;

        [SerializeField, Range(0, 10)] private float sprintSpeed;
        [SerializeField] protected bool sprint;
        protected float currentSpeed => rgd.velocity.magnitude;
        [HideInInspector] public Vector3 velocity => rgd.velocity;

        public bool isDead => Health == 0;

        protected Vector3 forward = Vector3.forward;

        private Vector3 force;
        private Vector3 moveVelocity;

        public Vector3 Force
        {
            get => force;
            set => force = value;
        }

        public void Sprint(bool sprinting)
        {
            sprint = sprinting;
        }

        public virtual void Move(Vector3 direction)
        {
            //only ever called on fixed update; called regardless if direction has a value or not
            //do we remove the y axis? or do we project it onto a plane based on the normal of the ground under us?
            if (direction.y != 0) direction.y = 0;

            force = Vector3.Lerp(force, Vector3.zero, Time.fixedDeltaTime);

            if (Math.Abs(direction.magnitude) > 0.05f)
                forward = direction.normalized;


            if (!rgd.isKinematic)
                rgd.MoveRotation(Quaternion.Slerp(rgd.rotation, Quaternion.LookRotation(forward, Vector3.up), 0.5f));

            moveVelocity = Vector3.Lerp(moveVelocity,
                direction.magnitude > 0
                    ? (sprint
                        ? Vector3.ClampMagnitude(moveVelocity + direction * movementAcceleration, sprintSpeed)
                        : Vector3.ClampMagnitude(moveVelocity + direction * movementAcceleration, maxSpeed))
                    : Vector3.zero, 0.5f);

            if (!rgd.isKinematic)
                rgd.velocity = moveVelocity + force + rgd.velocity.y * Vector3.up;

            force.y = 0;
            /*
            if (currentSpeed < maxSpeed)
                rgd.AddForce(movementAcceleration * direction, ForceMode.VelocityChange);
            */


            if (isAnimatorNotNull && (moveVelocity.magnitude / maxSpeed < 0.001) || rgd.isKinematic)
            {
                anim.SetFloat(Movement, 0f);
            }
            else if (isAnimatorNotNull) anim.SetFloat(Movement, moveVelocity.magnitude / maxSpeed);
        }

        #endregion


        [SerializeField, HideInInspector] protected Rigidbody rgd;
        public Rigidbody Rigidbody => rgd;
        [SerializeField] protected UnityEngine.Animator anim;
        protected bool isAnimatorNotNull;

        private static readonly int
            Movement = UnityEngine.Animator.StringToHash("movement"); // property in animator to set anim speed

        private static readonly int Hurt = UnityEngine.Animator.StringToHash("hurt");

        protected static readonly int Light_Attack = UnityEngine.Animator.StringToHash("light attack");
        protected static readonly int Heavy_Attack = UnityEngine.Animator.StringToHash("heavy attack");
        protected static readonly int Interact1 = UnityEngine.Animator.StringToHash("interact");
        protected static readonly int Down = UnityEngine.Animator.StringToHash("down");
        private float timeSinceDamage = 100;
        private List<SkinnedMeshRenderer> damageRenderers;
        private MaterialPropertyBlock prop;

        //used to calculate the animation speed

        protected virtual void Awake()
        {
            isAnimatorNotNull = anim != null;

            damageRenderers = GetComponentsInChildren<SkinnedMeshRenderer>().ToList();

            prop = new MaterialPropertyBlock();
        }

        protected virtual void Reset()
        {
            rgd = !GetComponent<Rigidbody>() ? gameObject.AddComponent<Rigidbody>() : GetComponent<Rigidbody>();
        }

        /// <summary>
        /// Called upon the character dying
        /// </summary>
        protected abstract void HasDied();


        private void OnEnable()
        {
            GameManager.gameManager.Pause += Pause;
        }


        private void Pause(bool obj)
        {
            rgd.isKinematic = obj;
        }

        private void OnDisable()
        {
            GameManager.gameManager.Pause -= Pause;
            Died?.Invoke(this);
            rgd.isKinematic = false;
        }
    }
}