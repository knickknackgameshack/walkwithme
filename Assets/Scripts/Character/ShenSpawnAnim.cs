﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ShenSpawnAnim : MonoBehaviour
{
    public Puzzle.Switch puzzle;
    private bool once = true;
    public PlayableDirector pd;




    // Update is called once per frame
    void Update()
    {
        if (once && puzzle.state.State)
        {
            once = false;
            pd.Play();
        }

    }
}
