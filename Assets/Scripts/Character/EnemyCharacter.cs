﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using OdinSerializer.Utilities;
//using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.VFX;

namespace Character
{
    public class EnemyCharacter : Character
    {
        [SerializeField] public float attackRange;
        [SerializeField] public float viewDistance;

        [SerializeField] public float attackCooldown;
        [SerializeField] public float attackWarmup;

        [SerializeField] private VisualEffect visualEffect;
        [SerializeField] List<GameObject> wings;
        [SerializeField] private VisualEffect succ;

        public enum EnemyType
        {
            Standard,
            Heavy
        }

        [SerializeField] public EnemyType enemyType;

        [SerializeField] public AudioClip idleClip;
        [SerializeField] public AudioClip attackClip;
        [SerializeField] public AudioClip damageClip;
        [SerializeField] public AudioClip deathClip;
        [SerializeField] public AudioSource idleSource;
        [SerializeField] public AudioSource activeSource;


        //can only hit one player at a time, so only need one spot
        private Collider[] colliders = new Collider[1];

        public bool IsSuckingSoul => isSuckingSoul;

        private bool isSuckingSoul;
        private Soul suckingSoul = null;
        private bool timeReverse;

        private static readonly int Attack1 = UnityEngine.Animator.StringToHash("attack");

        [SerializeField] private Renderer[] renderers;
        private MaterialPropertyBlock prop;
        private float time;
        [SerializeField] private Collider coll;

        public Action Started;
        private bool started;

        protected override void Awake()
        {
            base.Awake();

            prop = new MaterialPropertyBlock();
            succ.gameObject.SetActive(false);
        }

        protected override void Update()
        {
            base.Update();
            renderers.ForEach(r =>
            {
                r.GetPropertyBlock(prop);
                prop.SetFloat("_time", time);
                r.SetPropertyBlock(prop);
            });

            time += timeReverse ? -Time.deltaTime : Time.deltaTime;

            if (time > 1.4f && !started)
            {
                started = true;
                Started?.Invoke();
            }
            if (isSuckingSoul)
            {
                succ.gameObject.SetActive(true);
                succ.gameObject.transform.position = suckingSoul.gameObject.transform.position;
                succ.gameObject.transform.LookAt(this.transform);
            }
            if (!isSuckingSoul)
            {
                succ.gameObject.SetActive(false);
                
            }
        }

        public void Attack()
        {
            int size = Physics.OverlapBoxNonAlloc(transform.forward + transform.position,
                new Vector3(1.5f, 20, 1.5f), colliders, transform.rotation, 1 << 10); //TODO : fix original reference

            if (isAnimatorNotNull) anim.SetTrigger(Attack1);

            if (size != 0)
            {
                var enemy = colliders.First().GetComponent<PlayerCharacter>();
                if (enemy is Soul soul)
                {
                    //if soul isn't down, and soul isn't invincible, we will down soul so suck
                    if (!isSuckingSoul && !soul.IsDown && !soul.Invincible)
                    {
                        enemy.Health -= 1;
                        isSuckingSoul = true;
                        suckingSoul = soul;
                    }
                }
                else
                {
                    enemy.Health -= 1;
                }
            }
        }

        protected override void HasDied()
        {
            if (Health > 0) return;

            StopAllCoroutines();
            
            visualEffect.gameObject.SetActive(true);
            
            //reverse time for fade out
            timeReverse = true;
            time = 2.2f;

            base.Update();
            renderers.ForEach(r =>
            {
                r.GetPropertyBlock(prop);
                prop.SetFloat("_speed", 0.5f);
                r.SetPropertyBlock(prop);
            });

            rgd.useGravity = false;
            coll.enabled = false;

            AudioManager.Instance.StopSFX(idleSource);
            activeSource.clip = deathClip;
            AudioManager.Instance.PlaySFX(activeSource);
            if (isSuckingSoul)
            {
                suckingSoul.Revive();
                isSuckingSoul = false;
            }

            if (anim != null)
            {
                anim.SetTrigger(Down);
            }

            Destroy(gameObject, 7f);
            //wings.ForEach(w => Destroy(w, 2.5f));
        }
    }
}