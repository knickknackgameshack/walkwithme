﻿using System;
using Game;
using Narrative;
using UnityEngine;

namespace Character
{
    public abstract class PlayerCharacter : Character
    {
        private static readonly int Wave = UnityEngine.Animator.StringToHash("Wave");
        private static readonly int Pose = UnityEngine.Animator.StringToHash("Pose");
        private static readonly int Point = UnityEngine.Animator.StringToHash("Point");

        public Action<PlayerCharacter> Interacted;


        [SerializeField] protected AudioSource interactionsSoundSource;

        public enum EmoteType
        {
            Wave,
            Pose,
            Point
        }

        protected override void Awake()
        {
            isInteractionSoundSourceNotNull = interactionsSoundSource != null;


            _interactImage.gameObject.SetActive(false);

            base.Awake();
        }

        //[SerializeField] protected float dashForce;
        //protected bool dashing;

        [HideInInspector] public bool Interacting;

        [SerializeField] protected Canvas _interactImage;

        protected bool isInteractionSoundSourceNotNull;
        //public abstract void Dash();

        public override void Move(Vector3 direction)
        {
            if (Interacting) return;
            base.Move(direction);
        }

        /// <summary>
        /// Basis for the interact use, determines whether the narrative interact or puzzle interact is used 
        /// </summary>
        public abstract void Interact();


        private void FixedUpdate()
        {
            //there is something that the player can interact with
            //show image for that
            //_interactImage.gameObject.SetActive(Interacted != null);
            _interactImage.transform.LookAt(GameManager.gameManager.cameraController.camera.transform.position);
        }

        public void Emote(EmoteType type)
        {
            if (!isAnimatorNotNull) return;

            switch (type)
            {
                case EmoteType.Wave:
                    anim.SetTrigger(Wave);
                    break;
                case EmoteType.Pose:
                    anim.SetTrigger(Pose);
                    break;
                case EmoteType.Point:
                    anim.SetTrigger(Point);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        protected void HandHold(Transform thisTransform, Transform otherTransform)
        {
            if (GameManager.gameManager.State == State.InGame &&
                Vector3.Distance(thisTransform.position, otherTransform.position) < 1.5f)
            {
                var targetPoint = (thisTransform.position + otherTransform.position) * 0.5f + Vector3.up;
                //facing the same way
                //find which is the left one
                var localPos = thisTransform.InverseTransformPoint(otherTransform.position);
                if (localPos.x > 0) //other to the right
                    anim.SetIKPosition(AvatarIKGoal.RightHand, targetPoint);
                else //other pos to the left
                    anim.SetIKPosition(AvatarIKGoal.LeftHand, targetPoint);
            }
        }
    }
}