﻿using UnityEngine;

namespace Character
{
    public class BossCharacter : Character
    {
        public enum Stage
        {
        }

        [SerializeField] private Stage stage;

        public void Attack()
        {
        }

        protected override void HasDied()
        {
            Destroy(gameObject);
        }
    }
}