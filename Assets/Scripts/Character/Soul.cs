﻿using System.Collections;
using Game;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.VFX;

namespace Character
{
    public class Soul : PlayerCharacter
    {
        public bool IsDown => downed;
        private bool downed;
        private float timeDown;
        [SerializeField] private float downTimeTillDeath;

        [SerializeField] public AudioClip footstepsGrassCobble;
        [SerializeField] public AudioClip footstepsTile;
        [SerializeField] public AudioClip footstepsDirt;
        [SerializeField] public AudioClip footstepsCarpet;
        [SerializeField] public AudioSource footstepsSoul;
        [SerializeField] public VisualEffect lightPillar;


        public override void Move(Vector3 direction)
        {
            if (!downed)
                base.Move(direction);
        }

        /// <summary>
        /// Soul's general Interact, based on context will activate either the narrative interact or the puzzle interact
        /// If both are valid at the current position, puzzle interact will be used 
        /// </summary>
        public override void Interact()
        {
            Interacted?.Invoke(this);
        }


        public string GetMaterial()
        {
            if (Physics.Raycast(transform.position, transform.up * -1, out var hit, 2f, 1 << (int) Layer.Floor))
            {
                return hit.transform.GetComponentInChildren<Renderer>()?.sharedMaterial.name ?? "";
            }

            return "";
        }

        protected override void HasDied()
        {
            downed = true;
            if (isAnimatorNotNull) anim.SetBool(Down, true);
            StartCoroutine(Downed());
        }

        /// <summary>
        /// Used by the reaper to revive the soul
        /// </summary>
        public void Revive()
        {
            Health = 1;
            downed = false;
            lightPillar.SendEvent("GetUpLights");
        }

        /// <summary>
        /// Tracks time while down, when time reaches the designated time, the soul perishes
        /// If the soul was revived before that, it exits gracefully
        /// </summary>
        /// <returns></returns>
        public IEnumerator Downed()
        {
            timeDown = 0;
            do
            {
                yield return null;
                rgd.isKinematic = true;
                rgd.constraints = RigidbodyConstraints.FreezeRotation;
                timeDown += Time.deltaTime;
                if (timeDown > downTimeTillDeath) downed = false;
            } while (downed);

            if (Health == 0)
            {
                downed = false;
                GameManager.GameOver(GameResult.SoulDied);
            }
            else anim.SetBool(Down, false);
            rgd.isKinematic = false;
            rgd.constraints = RigidbodyConstraints.None;
            rgd.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }

        public void EnableReaperLegoHead()
        {
            //Debug.Log("SLHE");
            _interactImage.gameObject.SetActive(true);
        }

        public void DisableReaperLegoHead()
        {
             //Debug.Log("SLHD");
            _interactImage.gameObject.SetActive(false);
        }
    }
}