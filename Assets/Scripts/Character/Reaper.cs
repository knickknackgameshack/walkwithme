﻿using System;
using System.Collections;
using Game;
using Narrative;
using Puzzle;
using UnityEngine;
using UnityEngine.VFX;

namespace Character
{
    public class Reaper : PlayerCharacter
    {
        private enum AttackType
        {
            Light,
            Heavy
        }

        private Collider[] hitEnemy = new Collider[4];

        public enum CorruptionLevel
        {
        }

        [SerializeField] private CorruptionLevel corruptionLevel;
        [SerializeField] private float attackModifier;

        [SerializeField] private float lightAttackDelay;
        [SerializeField] private float lightAttackWindUp;
        [SerializeField] private float heavyAttackDelay;
        [SerializeField] private float heavyAttackWindUp;

        [SerializeField] private float timeTillAttack;
        [SerializeField] private AttackType lastAttack;
        [SerializeField] private float stunTime;
        [SerializeField] private float parryTime;
        private bool stunned;
        private bool parrying;
        private float timeSinceAttack;
        [SerializeField] private GameObject lightVisual;
        [SerializeField] private GameObject heavyVisual;

        [Space] [Header("Health")] public float HealingProximity;
        private float TimeSinceHeal;
        public float TimeToHeal;

        [Space, Header("Interaction Sound Clips")] [SerializeField]
        private AudioClip lightAttackSoundClip;

        [SerializeField] private AudioClip heavyAttackSoundClip;
        [SerializeField] private AudioClip takeDamageSoundClip;

        [Header("Puzzle Status")] [HideInInspector]
        public GateInteractive Gate;

        public EmotiveView emotiveView;
        private bool show;
        [SerializeField] private VisualEffect sparkEffect;
        [SerializeField] private VisualEffect glyphEffect;
        [SerializeField] private TrailRenderer trailEffect;


        public void Start()
        {
            if (trailEffect != null)
                trailEffect.gameObject.SetActive(false);
        }


        protected override void HasDied()
        {
            if (!stunned)
                StartCoroutine(Stunned());
        }

        public IEnumerator Stunned()
        {
            stunned = true;
            rgd.isKinematic = true;
            rgd.constraints = RigidbodyConstraints.FreezeRotation;
            if (isAnimatorNotNull) anim.SetBool(Down, true);
            yield return new WaitForSeconds(stunTime);
            Health = 1;
            if (isAnimatorNotNull) anim.SetBool(Down, false);
            rgd.isKinematic = false;
            rgd.constraints = RigidbodyConstraints.None;
            rgd.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            stunned = false;
        }

        /// <summary>
        /// Reaper's parry, enables invincibility for a short while to block incoming attacks
        /// </summary>
        public void Parry()
        {
            if (!parrying && !invincible)
            {
                StartCoroutine(ParryTimer());
            }
        }


        public void CheckPuzzleStatus()
        {
            if (Gate == null)
            {
                //todo signify there is no gate stored
            }
            else
            {
                Gate.ShowStatus(this);
            }
        }

        private IEnumerator ParryTimer()
        {
            parrying = true;
            invincible = true;
            yield return new WaitForSeconds(invincibleTime);
            invincible = false;
            yield return new WaitForSeconds(parryTime);
            parrying = false;
        }

        /// <summary>
        /// Reapers Light Attack
        /// </summary>
        public void LightAttack()
        {
            if ((timeTillAttack <= 0 && !invincible) ||
                (lastAttack == AttackType.Heavy && timeTillAttack <= heavyAttackDelay / 2 && !invincible))
            {
                var size = Physics.OverlapBoxNonAlloc(transform.forward + transform.position + transform.up * 0.5f,
                    new Vector3(1, 0.5f, 1), hitEnemy, transform.rotation,
                    (1 << ((int) Layer.Enemy) | (1 << (int) Layer.Breakable)));


                if (isInteractionSoundSourceNotNull)
                {
                    interactionsSoundSource.clip = lightAttackSoundClip;
                    interactionsSoundSource.Play();
                }

                timeTillAttack = lightAttackDelay;
                lastAttack = AttackType.Light;

                if (isAnimatorNotNull) anim.SetTrigger(Light_Attack);
                StartCoroutine(LightSmack(size));
                

                timeSinceAttack = 0;
                //GameManager.gameManager.State = State.Combat;
            }
        }

        IEnumerator LightSmack (int size)
        {
            rgd.isKinematic = true;
            //rgd.freezeRotation = true;
            rgd.constraints = RigidbodyConstraints.FreezeRotation;
            yield return new WaitForSeconds(lightAttackWindUp);
            for (int i = 0; i < size; i++)
            {
                var enemy = hitEnemy[i].GetComponentInParent<EnemyCharacter>();
                if (enemy == null)
                {
                    var breakable = hitEnemy[i].GetComponent<BreakableObject>();
                    breakable.Break();
                }
                else
                {
                    enemy.Health -= 1;
                    sparkEffect.SendEvent("StartSparks");
                    if (enemy.Health >= 1)
                    {
                        enemy.activeSource.Stop();
                        enemy.activeSource.clip = enemy.damageClip;
                        enemy.activeSource.Play();
                    }
                }
            }
            yield return new WaitForSeconds(lightAttackDelay- lightAttackWindUp);
            rgd.isKinematic = false;
            rgd.constraints = RigidbodyConstraints.None;
            rgd.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            //rgd.freezeRotation = false;

        }

        public override void Move(Vector3 direction)
        {
            if (stunned || timeSinceAttack < 2) return;
            base.Move(direction);
        }

        /// <summary>
        /// Reapers Heavy Attack
        /// </summary>
        public void HeavyAttack()
        {
            if ((timeTillAttack <= 0 && !invincible) ||
                (lastAttack == AttackType.Light && timeTillAttack <= lightAttackDelay / 2 && !invincible))
            {
                var size = Physics.OverlapSphereNonAlloc(transform.position + transform.up * 0.5f, 2.5f, hitEnemy,
                    (1 << ((int) Layer.Enemy) | (1 << (int) Layer.Breakable)));

                if (isInteractionSoundSourceNotNull)
                {
                    interactionsSoundSource.clip = heavyAttackSoundClip;
                    interactionsSoundSource.Play();
                }

                timeTillAttack = heavyAttackDelay;
                lastAttack = AttackType.Heavy;
                

                if (isAnimatorNotNull) anim.SetTrigger(Heavy_Attack);

                StartCoroutine(HeavySmack(size));

                timeSinceAttack = 0;
                //GameManager.gameManager.State = State.Combat;
            }
        }

        IEnumerator HeavySmack( int size)
        {
            rgd.isKinematic = true;
            rgd.constraints = RigidbodyConstraints.FreezeRotation;
            yield return new WaitForSeconds(heavyAttackWindUp);
            for (int i = 0; i < size; i++)
            {
                var enemy = hitEnemy[i].GetComponentInParent<EnemyCharacter>();
                if (enemy == null)
                {
                    var breakable = hitEnemy[i].GetComponent<BreakableObject>();
                    breakable.Break();
                }
                else
                {
                    enemy.Health -= 2;
                    sparkEffect.SendEvent("StartSparks");
                    if (enemy.Health >= 1)
                    {
                        enemy.activeSource.Stop();
                        enemy.activeSource.clip = enemy.damageClip;
                        enemy.activeSource.Play();
                    }
                }
            }
            yield return new WaitForSeconds(heavyAttackDelay- heavyAttackWindUp);
            rgd.isKinematic = false;
            rgd.constraints = RigidbodyConstraints.None;
            rgd.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        }

        public override void Interact()
        {
            Interacted?.Invoke(this);
            if (Interacted != null && isAnimatorNotNull)
            {
                anim.SetTrigger(Interact1);
                Debug.Log("interact");
                glyphEffect.SendEvent("GetGlyphs");
            }
        }

        public void EnableSoulLegoHead()
        {
            //Debug.Log("SLHE");
            _interactImage.gameObject.SetActive(true);
        }

        public void DisableSoulLegoHead()
        {
            // Debug.Log("SLHD");
            _interactImage.gameObject.SetActive(false);
        }

        private void FixedUpdate()
        {
            if (timeTillAttack > 0) timeTillAttack -= Time.fixedDeltaTime;
            timeSinceAttack += Time.fixedDeltaTime;

            //if (timeSinceAttack > 5) GameManager.gameManager.State = State.InGame;

            if (
                Health < MaxHealth &&
                TimeSinceHeal > TimeToHeal &&
                Vector3.Distance(
                    transform.position,
                    GameManager.gameManager.Soul.transform.position
                ) < HealingProximity
            )
            {
                TimeSinceHeal = 0;
                Health++;
            }
            else if (Health < MaxHealth) TimeSinceHeal += Time.fixedDeltaTime;

            if (trailEffect != null)
            {
                if (timeSinceAttack < 1f)
                    trailEffect.gameObject.SetActive(true);
                else
                    trailEffect.gameObject.SetActive(false);
            }
        }

        public void InteractHold(bool b)
        {
            //there is a gate, and no other interactions and we are holding the button
            if (b == true && Interacted == null && Gate != null)
            {
                //show = true;
                Gate.ShowStatus(this);
            }
            else
            {
                //show = false;
                emotiveView.HidePuzzle();
            }
        }
    }
}