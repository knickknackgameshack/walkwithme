﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] private Slider masterSlider;

    [SerializeField] private Slider musicSlider;

    [SerializeField] private Slider SFXSlider;

    public void ChangeMasterVolume()
    {
        AudioManager.Instance.SetMasterVolume(masterSlider.value);
    }

    public void ChangeMusicVolume()
    {
        AudioManager.Instance.SetMusicVolume(musicSlider.value);
    }

    public void ChangeSFXVolume()
    {
        AudioManager.Instance.SetSFXVolume(SFXSlider.value);
    }
}