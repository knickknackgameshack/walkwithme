﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI
{
    public class WorldSpaceCanvasObject : MonoBehaviour
    {
        public TextMeshProUGUI textBox;
        private Camera cameraToLookAt;
        public void Start()
        {
            cameraToLookAt = Camera.main;
        }
        public void Update()
        {
            Vector3 v = cameraToLookAt.transform.position - transform.position;
            v.x = v.z = 0.0f;
            transform.LookAt(cameraToLookAt.transform.position - v);
            transform.Rotate(0, 180, 0);
        }
    }
}