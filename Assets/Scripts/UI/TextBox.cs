﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBox : MonoBehaviour
{
    // Start is called before the first frame update
    private Image textBox;

    void Start()
    {
        textBox = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            //TextboxToggle();
        }
    }

    private void TextboxToggle()
    {
        if (textBox.enabled == true)
            textBox.enabled = false;
        else
            textBox.enabled = true;
    }
}