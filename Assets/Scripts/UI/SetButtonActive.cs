﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SetButtonActive : UnityEngine.MonoBehaviour
    {
        [SerializeField] private Button buttonSetOnAwake;

        private void Awake()
        {
            buttonSetOnAwake.Select();
        }

        public void SetButton()
        {
            buttonSetOnAwake.Select();
        }
    }
}