﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenSpaceCanvas : MonoBehaviour
{
    private static ScreenSpaceCanvas instance;

    public static ScreenSpaceCanvas Instance
    {
        get { return instance; }
    }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void canvasAppear()
    {
        if (instance.enabled == true)
        {
            instance.enabled = false;
        }
        else
        {
            instance.enabled = true;
        }
    }
}