﻿using System;
using Game;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextHolderUI : MonoBehaviour
    {
        private TextMeshProUGUI textField;

        [SerializeField] public string textDictionaryName;
        [SerializeField] public string textDictionaryKey;

        private void OnEnable()
        {
            textField = GetComponent<TextMeshProUGUI>();

            textField.text = GameManager.GetDialogue(textDictionaryName, textDictionaryKey);
        }
    }
}