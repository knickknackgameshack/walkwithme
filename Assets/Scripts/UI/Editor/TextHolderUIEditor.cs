﻿using System.Collections.Generic;
using System.Linq;
using Game;
using Narrative;
using UnityEditor;
using UnityEngine;

namespace UI.Editor
{
    [CustomEditor(typeof(TextHolderUI))]
    public class TextHolderUIEditor : UnityEditor.Editor
    {
        private string stage;
        private int choice;
        private int nameChoice;
        private List<TextDictionary> assets;


        private void OnEnable()
        {
            assets = GameManager.GetTextDictionaries();
        }


        public override void OnInspectorGUI()
        {
            var textHolder = target as TextHolderUI;

            var tempNames = assets.Select(n => n.name).ToArray();
            choice = tempNames.Length == 0 ? 0 : choice;

            choice = EditorGUILayout.Popup(choice, tempNames.Length > 0
                ? tempNames
                : new[] {""});

            var tempKeys = assets[choice].Text.Keys.ToArray();
            nameChoice = tempNames.Length == 0 ? 0 : nameChoice;
            nameChoice = EditorGUILayout.Popup(nameChoice, tempKeys.Length > 0
                ? tempKeys
                : new[] {""});

            if (tempKeys.Length != 0)
            {
                var temp = assets[choice].Text[tempKeys[nameChoice]][assets[choice].Language];

                temp = EditorGUILayout.TextArea(temp, GUILayout.MinHeight(60), GUILayout.ExpandHeight(true));

                if (temp != assets[choice].Text[tempKeys[nameChoice]][assets[choice].Language])
                {
                    assets[choice].Text[tempKeys[nameChoice]][assets[choice].Language] = temp;
                    EditorUtility.SetDirty(assets[choice]);
                }

                textHolder.textDictionaryName = assets[choice].name;
                textHolder.textDictionaryKey = tempKeys[nameChoice];
            }
        }
    }
}