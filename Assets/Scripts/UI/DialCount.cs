﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialCount : MonoBehaviour
{
    // Start is called before the first frame update
    public Puzzle.DialSwitch dial;
    public Puzzle.ActivatableDial ad;
    public TextMeshProUGUI tmi;
    public bool helper;


    // Update is called once per frame
    void Update()
    {
        if (ad.GetActivated)
        {
            tmi.enabled = true;
            tmi.text = (dial.currentIndex + 1).ToString();

            if (helper)
            {
                if (!dial.state.State)
                {
                    tmi.color = new Color(1, 0, 0, 1);
                    tmi.outlineColor = new Color(1, 0, 0, 0.4f);
                }
                else
                {
                    tmi.color = new Color(0, 1, 0, 1);
                    tmi.outlineColor = new Color(0, 1, 0, 0.4f);
                }
            }
            else
            {
                tmi.color = new Color(0, 0, 0, 1);
                tmi.outlineColor = new Color(1, 1, 1, 0.8f);
            }

        }
        else
        {
            tmi.enabled = false;
        }
    }
}
