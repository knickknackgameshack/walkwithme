﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Game;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioMixer mainMixer;

    [SerializeField] private AudioClip AmbientMusic;
    [SerializeField] private AudioClip CombatMusic;
    [SerializeField] private AudioClip BossMusic1;
    [SerializeField] private AudioClip BossMusic2;
    [SerializeField] private AudioClip MenuMusic;
    [SerializeField] public AudioSource AmbientSource;
    [SerializeField] public AudioSource CombatSource;
    [SerializeField] public AudioSource MenuSource;
    [SerializeField] public AudioSource GiantLoopingSFX;

    private Dictionary<AudioSource, float> sourceVolume;

    private static AudioManager instance;

    public static AudioManager Instance
    {
        get { return instance; }
    }

    private int trackTracker = 1;

    private void Awake()
    {
        instance = this;
        //MenuSource.Stop();
        MenuSource.clip = MenuMusic;
        GiantLoopingSFX.volume = 0;

        AmbientSource.clip = AmbientMusic;
        CombatSource.clip = CombatMusic;
        MenuSource.clip = MenuMusic;


        sourceVolume = new Dictionary<AudioSource, float>
        {
            {AmbientSource, AmbientSource.volume },
            {CombatSource, CombatSource.volume },
            {MenuSource, MenuSource.volume },
        };
    }
    private void Start()
    {
        //AmbientSource.Stop();
        //CombatSource.Stop();
    }

    private void Update()
    {
        switch (GameManager.gameManager.State)
        {
            case State.InGame:
                if (trackTracker !=2) //(AmbientSource.isPlaying == false)
                {
                    trackTracker = 2;

                    StopAllCoroutines();
                    StartCoroutine(FadeOutMusic(CombatSource, 2.5f));
                    StartCoroutine(FadeOutMusic(MenuSource, 2.5f));
                    StartCoroutine(FadeInMusic(AmbientSource, 2.5f));
                    //PlayAmbient();
                }
                return;

            case State.Combat:
                if (trackTracker != 3) //(CombatSource.isPlaying == false)
                {
                    trackTracker = 3;
                    if (CombatSource.isPlaying == false) CombatSource.Play();
                    StopAllCoroutines();
                    StartCoroutine(FadeOutMusic(AmbientSource, 2.5f));
                    StartCoroutine(FadeOutMusic(MenuSource, 2.5f));
                    StartCoroutine(FadeInMusic(CombatSource, 2.5f));
                    //PlayCombat();
                }
                return;

            case State.Menu:
                if (trackTracker != 1) //(MenuSource.isPlaying == false)
                {
                    trackTracker = 1;

                    StopAllCoroutines();
                    StartCoroutine(FadeOutMusic(AmbientSource, 2.5f));
                    StartCoroutine(FadeOutMusic(CombatSource, 2.5f));
                    StartCoroutine(FadeInMusic(MenuSource, 2.5f));
                    //
                }
                return;

            default:
                return;
        }


    }

    public void PlaySFX(AudioSource source)
    {
        source.Play();
    }
    public void StopSFX(AudioSource source)
    {
        source.Stop();
    }

    public void FadeInAmbientSFX(float fadeTime)
    {
        StartCoroutine(FadeInSFX(GiantLoopingSFX, fadeTime));
    }

    public void FadeOutAmbientSFX(float fadeTime)
    {
        StartCoroutine(FadeOutSFX(GiantLoopingSFX, fadeTime));
    }


    public IEnumerator FadeInMusic(AudioSource source, float fadeTime)
    {
        source.volume = 0;

        float fadeAmount = 0;

        while (fadeAmount < 1)
        {
            fadeAmount += Time.deltaTime / fadeTime;

            source.volume = Mathf.Lerp(0, sourceVolume[source], fadeAmount);

            ///source.volume += Time.deltaTime / fadeTime;
            yield return null;
        }
        //source.Play();
    }
    public IEnumerator FadeOutMusic(AudioSource source, float fadeTime)
    {
        float tempVol = source.volume;

        float fadeAmount = 0;
        while (source.volume > 0f)
        {
            fadeAmount += Time.deltaTime / fadeTime;

            source.volume = Mathf.Lerp(sourceVolume[source], 0, fadeAmount);
            yield return null;
        }
        //source.Stop();
    }

    public IEnumerator FadeInSFX(AudioSource source, float fadeTime)
    {
        float tempVol = 1f;
        source.volume = 0;
        while (source.volume <= tempVol)
        {
            source.volume += Time.deltaTime / fadeTime;
            yield return null;
        }
    }
    public IEnumerator FadeOutSFX(AudioSource source, float fadeTime)
    {
        float tempVol = source.volume;
        while (source.volume > 0f)
        {
            source.volume -= tempVol * Time.deltaTime / fadeTime;
            yield return null;
        }
    }

    public void PlayAmbient()
    {
        AmbientSource.clip = AmbientMusic;
        AmbientSource.Play();
    }
    public void PlayCombat()
    {
        CombatSource.clip = CombatMusic;
        CombatSource.Play();
    }

    public void PlaySong(AudioSource source, AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }

    public void SetMasterVolume(float vol)
    {
        mainMixer.SetFloat("MasterVolume", vol);
    }

    public void SetMusicVolume(float vol)
    {
        mainMixer.SetFloat("MusicVolume", vol);
    }

    public void SetSFXVolume(float vol)
    {
        mainMixer.SetFloat("SFXVolume", vol);
    }
}