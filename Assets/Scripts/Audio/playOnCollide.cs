﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playOnCollide : MonoBehaviour
{
    private bool yes = true;

    private void OnTriggerEnter(Collider other)
    {
        if (yes)
        {
            this.GetComponent<AudioSource>().Play();
            Destroy(gameObject, 11);
            yes = !yes;
        }
    }
}
