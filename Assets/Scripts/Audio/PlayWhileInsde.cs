﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayWhileInsde : MonoBehaviour
{
    [SerializeField] private AudioSource source;

    private void OnTriggerEnter(Collider other)
    {
        StopAllCoroutines();
        StartCoroutine(FadeInMusic(source, 2.5f));
    }

    private void OnTriggerExit(Collider other)
    {
        StopAllCoroutines();
        StartCoroutine(FadeOutMusic(source, 2.5f));
    }

    public IEnumerator FadeInMusic(AudioSource source, float fadeTime)
    {
        source.volume = 0;

        float fadeAmount = 0;

        while (fadeAmount < 1)
        {
            fadeAmount += Time.deltaTime / fadeTime;

            source.volume = Mathf.Lerp(0, 100, fadeAmount);

            ///source.volume += Time.deltaTime / fadeTime;
            yield return null;
        }
        //source.Play();
    }
    public IEnumerator FadeOutMusic(AudioSource source, float fadeTime)
    {
        float tempVol = source.volume;

        float fadeAmount = 0;
        while (source.volume > 0f)
        {
            fadeAmount += Time.deltaTime / fadeTime;

            source.volume = Mathf.Lerp(100, 0, fadeAmount);
            yield return null;
        }
        //source.Stop();
    }
}
